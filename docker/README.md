# Creación de la imagen

[Crear](https://docs.docker.com/engine/reference/commandline/build) la imagen **rrhh-frontend**:

```sh
docker build -t rrhh-frontend -f docker/Dockerfile .
```

# Uso de la imagen

## Despliegue básico

Se puede levantar un simple contenedor de prueba mediante el comando:

```sh
docker run --name rrhh-frontend -d rrhh-frontend
```

## Exposición de puerto externo

```sh
docker run --name rrhh-frontend -d -p 8000:8000 rrhh-frontend
```

En este caso puedes acceder al servicio mediante la dirección [http://127.0.0.1:8000](http://127.0.0.1:8000) de tu navegador.

## Configuración completa

Se puede configurar el servicio mediante la siguiente variable de entorno:

```sh
# URL del servicio de backend de rrhh. (por defecto: http://127.0.0.1:3000)
BACKEND_URL=http://127.0.0.1:3000
# URL del servicio de feriados de rrhh. (por defecto: http://127.0.0.1:4000)
FERIADOS_URL=http://127.0.0.1:4000
# URL del servicio de biométricos de rrhh. (por defecto: http://127.0.0.1:5000)||
BIOMETRICOS_URL=http://127.0.0.1:5000
```

Para asignar valores de variables de entorno diferentes puedes levantar el contenedor con las opciones **-e** :

```sh
docker run --name rrhh-frontend -d -p 8000:8000 -e BACKEND_URL=http://1.2.3.4:1111 -e FERIADOS_URL=http://1.2.3.4:2222 -e BIOMETRICOS_URL=http://1.2.3.4:3333 rrhh-frontend
```

## Despliegue mediante docker-compose.yml

Puedes levantar el servicio mediante un archivo [docker-compose.yml](https://docs.docker.com/compose/compose-file) incluyendo por ejemplo las siguientes líneas:

```yaml
frontend:
    environment:
        - BACKEND_URL=http://127.0.0.1:3000
        - FERIADOS_URL=http://127.0.0.1:4000
        - BIOMETRICOS_URL=http://127.0.0.1:5000
    image: rrhh-frontend
    ports:
        - "8000:8000"
```

