#!/bin/sh

set -e

find /usr/share/nginx/html/ -type f -exec sed -i "s~BACKEND\_URL~${BACKEND_URL}~g" {} \;
find /usr/share/nginx/html/ -type f -exec sed -i "s~FERIADOS\_URL~${FERIADOS_URL}~g"  {} \;
find /usr/share/nginx/html/ -type f -exec sed -i "s~BIOMETRICOS\_URL~${BIOMETRICOS_URL}~g"  {} \;

exec "$@"
