Creando estructura de archivos con comandos
===========================================

#### - Instalando sub tareas del generador yeoman
    npm install -g generator-gulp-angular-sub
Documentación: https://www.npmjs.com/package/generator-gulp-angular-sub

    npm install -g generator-gulp-angular-subtask 
Documentación: https://github.com/doronsever/generator-gulp-angular-subtask

#### - Comando para crear una vista html con su controlador y configuración de su ruta en index.route.js (Este el principal comando que se deberá usar para crear vistas)
    yo gulp-angular-sub:view

#### - **Comando para crear un componente con directivas (No se recomienda usar este comando ya que crea componentes en base a directivas)**
    yo gulp-angular-sub:component


Instalación desde cero para crear una estructura con el generador de gulp-angular (Solo para cuando se necesite crear una nueva base para el frontend)
======================

#### - Instalar herramientas requeridas: yo, gulp, bower

    sudo npm install -g yo gulp bower

#### - Instalar el generator gulp-angular:

    sudo npm install -g generator-gulp-angular

#### - Habilitar permisos para el usuario para instalar los bower_components, colocar el username del equipo en caso que se requiera (Opcional)
    sudo chown -R <username>:<username> /home/<username>/.config/configstore

#### - Crear el proyecto con el generador gulp-angular
    sudo yo gulp-angular

#### - Iniciar el proyecto
    sudo gulp serve