# Sistema de Control de Personal de Recursos Humanos (SCPRRHH) -FRONTEND

## Introducción
El presente Sistema nos permite realizar el control de marcaciones, permisos, designación de horarios para el cumplimiento de las horas laborales.

Cumple con las siguientes funcionalidades:

Roles de Usuario
- Rol de Usuario Super Administrador
- Rol de Usuario Recursos Humanos (RRHH)
- Rol de Usuario Jefe de Unidad
- Rol de Usuario Funcionario

Gestión de diversos tipos de Horarios
- Establecer un Horario en el sistema
- Establecer un Rango de Horas para un Horario
- Designación de un horario a un funcionario
- Registro de Horarios Manual

Permisos
- Designación de permiso
- Existen 3 tipos de Permisos Licencia, Comisión, Particular
- Impresión de Boletas de Permiso
- Rol Funcionario
    - Puede crear/editar/eliminar su permiso
- Rol Jefe
    - Puede aprobar/rechazar un permiso
- Rol Recursos Humanos (RRHH)
    - Puede crear/editar/eliminar toda la lista de permisos

Configuración
- Gestión de Unidad Organizacional
- Gestión de Feriados
- Gestión de Usuarios
- Configuración de Semana Laboral y asignación de Horarios por Defecto

Reporte
- Reporte detallado de marcaciones uno/todos usuarios
- Reporte consolidado de marcaciones de atrasos en minutos uno/todos usuarios

Sincronización
- Sincronización de datos de Marcación del Microservicio que recupera datos de los biometricos
- Sincronización de datos de Feriados del Microservicio que entrega Feriados
- Sincronización con usuarios de LDAP


#### Este sistema funciona con los siguientes Microservicios:
Microservicio que recupera datos de los biométricos
> https://gitlab.agetic.gob.bo/agetic/rrhh-biometricos

Microservicio que entrega Feriados
> https://gitlab.agetic.gob.bo/agetic/rrhh-feriados-backend

## Tecnologías Utilizadas
- AngularJS
- Angular Material

## Instalación de la aplicación
Para una correcta instalación del frontend, se debe tener las configuraciones necesarias, para ello se recomienda revisar los archivos:

* [Instalación del sistema](INSTALL.md)
* [(Opcional) Despliegue con Docker](docker/README.md)

## Manuales de usuarios
* [Superadministrador](docs/RrhhManualAdmin_v4.pdf)
* [Funcionario o usuario estandar](docs/RrhhManualAdmin_v4.pdf)
* [Jefe o supervisor](docs/RrhhManualFuncionario_v4.pdf)
* [RRHH](docs/RrhhManualRRHH_v4.pdf)

## Estructura del sistema
![Estructura del sistema](docs/estructura_sistema_v2.png)

## Licencia
LICENCIA PÚBLICA GENERAL de Consideraciones y Registro de Software Libre en Bolivia (LPG-Bolivia)
* [LICENCIA.md](LICENCIA.md)