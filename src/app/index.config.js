(function() {
'use strict';

angular
    .module('app')
    .config(config)
    .value('scDateTimeConfig', {
        defaultTheme: 'material',
        autosave: true,
        compact: true,
        displayTwentyfour: true
    })
    .value('scDateTimeI18n', {
        previousMonth: "Mes anterior",
        nextMonth: "Siguiente mes",
        incrementHours: "Incrementar horas",
        decrementHours: "Decrementar horas",
        incrementMinutes: "Incrementar Minutos",
        decrementMinutes: "Decrementar Minutos",
        switchAmPm: "Cambiar AM/PM",
        now: "Ahora",
        cancel: "Cancelar",
        save: "Guardar",
        weekdays: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        switchTo: 'Cambiar a',
        clock: 'Reloj',
        calendar: 'Calendario'
    });

    /** @ngInject */
    function config($logProvider, $authProvider, restUrl, authUrl, appName, $mdDateLocaleProvider, moment) {
        // Enable log
        $logProvider.debugEnabled(true);

        // Config auth
        $authProvider.loginUrl = authUrl;
        // $authProvider.signupUrl = "http://api.com/auth/signup";
        $authProvider.tokenName = "token";
        $authProvider.tokenPrefix = appName;
        // $authProvider.authToken = 'Bearer';

        $mdDateLocaleProvider.formatDate = function(date) {
            if (date) {
                return moment(date).format('DD/MM/YYYY');
            }
            return null;
        };

        $mdDateLocaleProvider.parseDate = function(dateString) {
            var m = moment(dateString, 'DD/MM/YYYY', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };

        // Spanish localization.
        $mdDateLocaleProvider.months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $mdDateLocaleProvider.shortMonths = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
        $mdDateLocaleProvider.days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
        $mdDateLocaleProvider.shortDays = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'];
        // Can change week display to start on Monday.
        $mdDateLocaleProvider.firstDayOfWeek = 1;
        // Optional.
        //$mdDateLocaleProvider.dates = [1, 2, 3, 4, 5, 6, 7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
        // In addition to date display, date components also need localized messages
        // for aria-labels for screen-reader users.
        $mdDateLocaleProvider.weekNumberFormatter = function(weekNumber) {
            return 'Semana ' + weekNumber;
        };
        $mdDateLocaleProvider.msgCalendar = 'Calendario';
        $mdDateLocaleProvider.msgOpenCalendar = 'Abrir calendario';
    }

})();
