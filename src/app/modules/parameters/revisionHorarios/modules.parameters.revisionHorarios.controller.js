(function() {
  'use strict';

  angular
    .module('app')
    .controller('RevisionHorariosController', RevisionHorariosController);

  /** @ngInject */
  function RevisionHorariosController(restUrl) {
    var vm = this;

    vm.title = 'Lista de revisiones de horario';
    vm.url = restUrl + 'registro_horarios_revision/';
    /*vm.fks = ['usuario', 'horario_rango'];
    vm.fields = [
      'id',
      'usuario',
      'fecha',
      'horario_rango',
      'hora_entrada',
      'hora_salida',
      'observacion'
    ];
    vm.order = [
      'usuario',
      'fecha',
      'hora_entrada',
      'hora_salida'
    ];*/
  }
})();
