(function() {
  'use strict';

  angular
    .module('app')
    .controller('DesignacionPermisosJefeController', DesignacionPermisosJefeController);

    /** @ngInject */
    function DesignacionPermisosJefeController(restUrl, Storage, DataService, $location) {
        var vm = this;
        var user = Storage.getUser();

        vm.title = 'Lista de Designación Permisos';
        vm.url = restUrl + 'designacion_permisos_funcionario/';
        vm.column = 2;
        vm.misPermisos = misPermisos;
        vm.fks = ['usuario', 'jefe', 'tipo_permiso', 'unidad_organizacional'];
        vm.fields = [
            'id',
            'usuario',
            'tipo_permiso',
            'motivo',
            'comision_lugar',
            'licencia_respaldo',
            'particular_numeral_iii',
            'particular_numeral_iv',
            'fecha_ini',
            'hora_ini',
            'fecha_fin',
            'hora_fin',
            'unidad_organizacional',
            'jefe'
            // 'fecha_registro'
        ];
        vm.permission = {
            print: true
        };
        vm.formly = [
            {
                key: 'motivo',
                hideExpression: "model.tipo_permiso == 'particular'"
            },
            {
                key: 'usuario',
                defaultValue: user.id,
                'templateOptions': {
                    disabled: true
                }
            },
            {
                key: 'comision_lugar',
                hideExpression: "model.tipo_permiso == 'particular' || model.tipo_permiso == 'licencia'"
            },
            {
                key: 'licencia_respaldo',
                hideExpression: "model.tipo_permiso == 'particular' || model.tipo_permiso == 'comision'"
            },
            {
                key: 'particular_numeral_iii',
                hideExpression: "model.tipo_permiso == 'comision' || model.tipo_permiso == 'licencia'"
            },
            {
                key: 'particular_numeral_iv', 
                hideExpression: "model.tipo_permiso == 'comision' || model.tipo_permiso == 'licencia'"
            },
            {
                key: 'fecha_ini',
                defaultValue: new Date()
            },
            {
                key: 'fecha_fin',
                defaultValue: new Date()
            },
            {
                key: 'hora_ini',
                defaultValue: '08:30',
                hideExpression: "model.tipo_permiso == 'licencia'"
            },
            {
                key: 'hora_fin',
                defaultValue: '19:00',
                hideExpression: "model.tipo_permiso == 'licencia'"
            },
            {
                key: 'estado',
                defaultValue: 'aprobado_rrhh'
            },
            {
                key: 'tipo_permiso',
                defaultValue: 'comision'
            }
        ];
        vm.template = 'app/modules/parameters/designacionPermisos/dialog.create.permiso.html';

        function misPermisos() {
            $location.path('');
            Storage.set('permisos', true);
        }
    }

})();
