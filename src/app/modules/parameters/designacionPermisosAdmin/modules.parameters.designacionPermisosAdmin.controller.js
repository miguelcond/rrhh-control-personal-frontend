(function() {
  'use strict';

  angular
    .module('app')
    .controller('DesignacionPermisosAdminController', DesignacionPermisosAdminController);

    /** @ngInject */
    function DesignacionPermisosAdminController(restUrl) {
        var vm = this;

        vm.title = 'Lista de Designación Permisos';
        vm.url = restUrl + 'designacion_permisos/';
        vm.column = 2;
        vm.fks = ['usuario', 'jefe', 'tipo_permiso', 'unidad_organizacional'];
        vm.idShow = true; // Mostrar el ID
        vm.fields = [
            'id',
            'usuario',
            'tipo_permiso',
            'estado',
            'motivo',
            'comision_lugar',
            'licencia_respaldo',
            // 'particular_numeral_iii',
            // 'particular_numeral_iv',
            'fecha_ini',
            'hora_ini',
            'fecha_fin',
            'hora_fin',
            'unidad_organizacional',
            'jefe',
            'observacion_rrhh',
            'fecha_registro'
        ];
        vm.permission = {
            print: true,
            delete: false
        };
        vm.formly = [
            {
                key: 'id',
                // label: 'Correlativo',
                'templateOptions': {
                    label: 'Correlativo'
                }
            },
            {
                key: 'motivo',
                hideExpression: "model.tipo_permiso == 'particular'"
            },
            {
                key: 'fecha_registro',
                hideExpression: "true"
            },
            {
                key: 'usuario',
                hideExpression: "true"
            },
            {
                key: 'comision_lugar',
                hideExpression: "model.tipo_permiso != 'comision'"
            },
            {
                key: 'licencia_respaldo',
                hideExpression: "model.tipo_permiso != 'licencia'"
            },
            {
                key: 'particular_numeral_iii',
                hideExpression: "true"
            },
            {
                key: 'particular_numeral_iv', 
                hideExpression: "true"
            },
            {
                key: 'fecha_ini',
                defaultValue: new Date()
            },
            {
                key: 'fecha_fin',
                defaultValue: new Date(),
                hideExpression: "model.tipo_permiso == 'olvido_marcar'"
            },
            {
                key: 'hora_ini',
                defaultValue: '08:30',
                hideExpression: "model.tipo_permiso == 'licencia'"
            },
            {
                key: 'hora_fin',
                defaultValue: '19:00',
                hideExpression: "model.tipo_permiso == 'licencia' || model.tipo_permiso == 'olvido_marcar'"
            },
            {
                key: 'tipo_permiso',
                defaultValue: 'comision'
            },
            {
                key: 'estado',
                defaultValue: 'aprobado_rrhh'
            }
        ];
        vm.template = 'app/modules/parameters/designacionPermisos/dialog.permiso.html';
        vm.dialogController = ['$scope', '$mdDialog', 'data', 'fields', 'title', 'add', 'column', 'done', 'Util', 'DataService', 'restUrl', DialogController];

        // vm.eventSave = function (data) {
        //  DataService.pdf(vm.url + 'print/' + data.id + '/')
        //     .then(function (data) {
        //         Modal.show({
        //             data: data,
        //             title: 'Impresión',
        //             templateUrl : 'app/components/crudTable/dialog.pdf.html'
        //         });
        //     });
        // }
    }

    function DialogController($scope, $mdDialog, data, fields, title, add, column, done, Util, DataService, restUrl) {
        var vm = $scope;

        vm.data = data;
        vm.fields = fields;
        vm.title = title;
        vm.add = add;
        vm.column = column;
        vm.querySearch = querySearch;
        vm.save = save;

        activate();

        function activate() {
            getUsers();
        }

        function getUsers() {
            var users = []
            DataService.list(restUrl + 'todos_usuarios/')
            .then(function(data) {
                if (data) {
                    for (var i in data) {
                        if (data[i].habilitado_marcar) {
                            users.push({
                                value: data[i].id,
                                display: data[i].username + ' - ' + data[i].first_name + ' ' + data[i].last_name
                            });                         
                        }
                    }
                    vm.funcionarios = users;
                    if (vm.data.usuario) {
                        vm.searchText = Util.getFkData(vm.fields, 'usuario', vm.data.usuario);
                    }
                }
            })
        }

        function querySearch (query) {
            return query ? vm.funcionarios.filter( createFilterFor(query) ) : vm.funcionarios;
        }

        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(state) {
                return (state.display.toLowerCase().indexOf(lowercaseQuery) !== -1);
            };
        }

        vm.hide = function() {
            $mdDialog.hide();
        }

        vm.cancel = function() {
            $mdDialog.cancel();
        }

        function save() {
            if (vm.selectedItem || data.usuario) {
                if (vm.selectedItem) {
                    data.usuario = vm.selectedItem.value;
                }
                delete data.autor;
                done('save', $mdDialog);
            }
        }
    }

})();
