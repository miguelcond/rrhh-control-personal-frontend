(function() {
    'use strict';

    angular
    .module('app')
    .controller('DesignacionVacacionesController', DesignacionVacacionesController)
    .controller('CalVacacionController', CalVacacionController);

    /** @ngInject */
    function DesignacionVacacionesController(Storage, DataService, restUrl, Modal, Message) {
        var vm = this;
        var user = Storage.getUser();

        vm.rol = user.groups[0];
        vm.lang = {
            estado: {
                'aprobado': 'Aprobado',
                'creado': 'Pendiente de aprobación',
                'rechazado': 'Rechazado'
            }
        };
        vm.aprobar = aprobar;
        vm.rechazar = rechazar;
        vm.reprogramar = reprogramar;
        vm.url = restUrl + 'designacion_vacacion_jefe/';
        vm.ver_tabla = false;

        activate();

        function activate() {
            getVacaciones();
        }

        function getVacaciones() {
            DataService.get(vm.url)
            .then(function (response) {
                if (response) {
                    var vacaciones = response;
                    var items = {};
                    for (var i in vacaciones) {
                        if (items[vacaciones[i].username]) {
                            items[vacaciones[i].username].items.push(vacaciones[i]);
                        } else {
                            items[vacaciones[i].username] = {
                                nombre_completo: vacaciones[i].usuario,
                                items: [vacaciones[i]]
                            };
                        }
                    }
                    vm.vacaciones = items;
                    vm.ver_tabla = Object.keys(items).length;
                }
            });
        }

        function aprobar(event, id) {
            Modal.confirm('¿Está seguro de aprobar la vacación?', function () {
                DataService.put(vm.url + id + '/', {estado: 'aprobado'})
                .then(function (response) {
                    if (response) {
                        Message.success("Vacación Aprobado");
                        getVacaciones();
                    }
                });
            });
        }

        function rechazar(event, id) {
            Modal.confirm('¿Está seguro de rechazar la vacación?', function () {
                DataService.put(vm.url + id + '/', {estado: 'rechazado'})
                .then(function (response) {
                    if (response) {
                        Message.success("Vacación Rechazado");
                        getVacaciones();
                    }
                });
            });
        }

        function reprogramar(event, id) {
            Modal.confirm('¿Está seguro de que el funcionario tiene que reprogramar su vacación?', function () {
                DataService.put(vm.url + id + '/', {estado: 'rechazado'})
                .then(function (response) {
                    if (response) {
                        Message.success("Vacación para Reprogramar");
                        getVacaciones();
                    }
                });
            });
        }
    }

    function CalVacacionController($scope, $compile, $timeout, uiCalendarConfig, FeriadosService, DataService, restUrl, Util, $log) {
        var vm = $scope;

        // var date = new Date();

        vm.year = Util.getGestion();

        var m = 0;
        var y = vm.year;

        vm.events = [];

        activate();

        function activate() {
            angular.element('body').on('click', 'button.fc-prev-button', function() {
                prevMounth();
            });

            angular.element('body').on('click', 'button.fc-next-button', function() {
                nextMounth();
            });
        }

        function nextMounth() {
            if (++m == 12) {
                m = 0;
                y++;
            }
            getFechas(m, y);
        }

        function prevMounth() {
            if (--m === 0) {
                m = 12;
                y--;
            }
            getFechas(m, y);
        }

        function getFechas(m, y) {
            DataService.get(restUrl + 'historico_vacacion_dependientes/?year=' + y + '&month=' + (m + 1))
            .then(function (response) {
                var events = response;
                for(var i in events) {
                    var fecha_ini = events[i].fecha_ini.split('-');
                    var fecha_fin = events[i].fecha_fin.split('-');
                    vm.addEvent({
                        title: events[i].usuario,
                        start: new Date(fecha_ini[0], fecha_ini[1] - 1, parseInt(fecha_ini[2])),
                        end: new Date(fecha_fin[0], fecha_fin[1] - 1, parseInt(fecha_fin[2]) + 1),
                        editable: false,
                        color: events[i].tipo_vacacion == 'completo' ? '#4C86A6' : '#f17e67'
                    });
                }
            });
        }

        /* event source that contains custom events on the scope */
        vm.events = [];

        /* alert on eventClick */
        vm.alertOnEventClick = function(date) {
            $log.debug(date.title + ' was clicked ');
        };
        /* alert on Drop */
        vm.alertOnDrop = function(event, delta){
            $log.debug('Event Dropped to make dayDelta ' + delta);
        };
        /* alert on Resize */
        vm.alertOnResize = function(event, delta){
            $log.debug('Event Resized to make dayDelta ' + delta);
        };
        /* add and removes an event source of choice */
        vm.addRemoveEventSource = function(sources,source) {
            var canAdd = 0;
            angular.forEach(sources,function(value, key){
                if(sources[key] === source){
                    sources.splice(key,1);
                    canAdd = 1;
                }
            });
            if(canAdd === 0){
                sources.push(source);
            }
        };
        /* add custom event */
        vm.addEvent = function(obj) {
            vm.events.push(obj);
        };
        /* remove event */
        vm.remove = function(index) {
            vm.events.splice(index,1);
        };
        /* Change View */
        vm.changeView = function(view,calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
        };
        /* Change View */
        vm.renderCalender = function(calendar) {
            $timeout(function() {
                if(uiCalendarConfig.calendars[calendar]){
                    uiCalendarConfig.calendars[calendar].fullCalendar('render');
                    getFechas(m, y);
                }
            }, 1000);
        };

        vm.renderCalender('calVacaciones');

        /* Render Tooltip */
        vm.eventRender = function( event, element ) {
            element.attr({'tooltip': event.title,
                'tooltip-append-to-body': true});
            $compile(element)(vm);
        };
        /* config object */
        vm.uiConfig = {
            calendar:{
                lang: 'es',
                editable: true,
                header:{
                    left: '',
                    center: 'title',
                    right: 'prev,next'
                },
                defaultDate: new Date(y, m, 1),
                eventClick: vm.alertOnEventClick,
                eventDrop: vm.alertOnDrop,
                eventResize: vm.alertOnResize,
                eventRender: vm.eventRender
            }
        };

        /* event sources array*/
        vm.eventSources = [vm.events];
    }
})();
