(function() {
	'use strict';

	angular
	.module('app')
	.controller('UnidadController', UnidadController);

	/** @ngInject */
	function UnidadController(restUrl) {
		var vm = this;

		vm.title = 'Unidades Organizacionales';
		vm.url = restUrl + 'unidad_organizacional/';

	}
})();
