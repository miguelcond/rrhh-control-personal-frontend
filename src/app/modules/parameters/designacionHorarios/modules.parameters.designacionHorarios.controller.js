(function() {
    'use strict';

    angular
    .module('app')
    .controller('DesignacionHorariosController', DesignacionHorariosController);

    /** @ngInject */
    function DesignacionHorariosController(restUrl) {
        var vm = this;

        vm.title = 'Lista de Designación de Horarios';
        vm.url = restUrl + 'designacion_horarios/';
        vm.fks = ['usuario', 'horario'];
        vm.fields = ['id', 'usuario', 'horario', 'fecha_ini', 'fecha_fin', 'designar_lunes', 'designar_martes', 'designar_miercoles', 'designar_jueves', 'designar_viernes', 'designar_sabado', 'designar_domingo'];
        vm.template = 'app/modules/parameters/designacionHorarios/dialog.horario.html';
        vm.column = 3;
        vm.formly = [
            {
                key: 'usuario',
                hideExpression: "true"
            },
            {
                key: 'fecha_ini',
                defaultValue: new Date()
            },
            {
                key: 'fecha_fin',
                defaultValue: new Date()
            },
            {
                key: 'designar_lunes',
                defaultValue: true
            },
            {
                key: 'designar_martes',
                defaultValue: true
            },
            {
                key: 'designar_miercoles',
                defaultValue: true
            },
            {
                key: 'designar_jueves',
                defaultValue: true
            },
            {
                key: 'designar_viernes',
                defaultValue: true
            }
        ];

        vm.dialogController = ['$scope', '$mdDialog', 'data', 'fields', 'title', 'add', 'column', 'done', 'Util', 'DataService', 'restUrl', 'Message', 'sincronizarUrl', 'Datetime', DialogController];

    }

    function DialogController($scope, $timeout, $mdDialog, data, fields, title, add, column, done, Util, DataService, restUrl, Message, sincronizarUrl, Datetime) {
        var vm = $scope;

        vm.data = data;
        vm.fields = fields;
        vm.title = title;
        vm.add = add;
        vm.column = column;
        vm.save = save;
        vm.todos = false;

        activate();

        function activate() {
            getUsers();
        }

        function getUsers() {
            var users = []
            DataService.list(restUrl + 'todos_usuarios/')
            .then(function(data) {
                if (data) {
                    for (var i in data) {
                        if (data[i].habilitado_marcar) {
                            users.push({
                                value: data[i].id,
                                display: data[i].username + ' - ' + data[i].first_name + ' ' + data[i].last_name
                            });
                        }
                    }
                    vm.funcionarios = users;

                    if (vm.data.usuario) {
                        if (Util.lengthOptions(vm.fields, 'usuario') == vm.data.usuario.length) {
                            vm.todos = true;
                        } else {
                            var usuarios = [];
                            vm.data.usuario.map(function (el) {
                                usuarios.push({
                                    "value": el
                                });
                            });
                            vm.data.usuario = usuarios;
                        }
                    }

                    $timeout(function () {
                        angular.element("#funcionarios").chosen();
                    }, 100);
                }
            })
        }

        vm.hide = $mdDialog.hide;
        vm.cancel = $mdDialog.cancel;

        function save() {
            if (vm.todos) {
                vm.data.usuario = [];
                vm.funcionarios.map(function (e) {
                    vm.data.usuario.push(e.value*1);
                });
                saveData(vm.data);
            } else {
                if (vm.data.usuario && vm.data.usuario.length) {
                    var usuarios = [];
                    vm.data.usuario.map(function (e) {
                        usuarios.push(e.value*1);
                    });
                    vm.data.usuario = usuarios;
                    saveData(vm.data);
                } else {
                    Message.warning('Debe seleccionar por lo menos un usuario');
                }
            }

        }

        function saveData(data) {
            var fecha_ini = data.fecha_ini;
            DataService.save(restUrl + 'designacion_horarios/', Util.parseSave(data))
            .then(function(response) {
                if (response) {
                    Message.success();
                    $mdDialog.hide();
                    sincronizar(fecha_ini);
                    $timeout(function () {
                        angular.element('#btn-refresh-crudtable').click();
                    }, 100);
                }
            });
        }

        function sincronizar (fecha) {
            var url = sincronizarUrl + '?fecha=' + Datetime.parseDate(fecha);
            // Message.warning('Se están sincronizando los biométricos, espere a que la operación termine antes de continuar.', null, 0);
            // angular.element('#loading-progress').show();
            DataService.get(url)
            .then(function (data) {
                // angular.element('#loading-progress').hide();
                if (data) {
                    Message.success(data);
                }
            })
            .catch(function () {
                // angular.element('#loading-progress').hide();
                Message.error('Ocurrió un error en la sincronización, vuelva a intentarlo más tarde');
            });

        }
    }

})();
