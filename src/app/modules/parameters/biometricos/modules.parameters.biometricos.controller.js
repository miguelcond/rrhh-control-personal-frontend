(function() {
	'use strict';

	angular
	.module('app')
	.controller('BiometricosController', BiometricosController);

	/** @ngInject */
	function BiometricosController() {
		var vm = this;

		// vm.url = 'http://192.168.27.205:5000/api/v1/biometricos/';
		vm.url = 'http://192.168.20.219:5000/api/v1/biometricos/';
		vm.title = 'Lista de Biométricos';
		vm.fields = [
			'id',
			'biometrico_url',
			'usuario',
			'contrasena'
		];

	}
})();
