(function() {
  'use strict';

  angular
    .module('app')
    .service('FeriadosService', FeriadosService)
    .controller('FeriadosController', FeriadosController)
    .controller('CalFeriadoController', CalFeriadoController);

  function FeriadosService($http, Message, calendarUrl) {
    var service = {
      getFeriados: getFeriados,
      getFeriadosGestion: getFeriadosGestion
    };

    return service;

    function getFeriados(m, y) {
      return $http.get(calendarUrl + 'v1/feriados?pais=BO&ano=' + y + '&mes=' + (++m))
        .then(function (response) {
          return response.data;
        }).catch(function (error) {
          Message.error(error.data || 'No se pudo establecer conexión con el servicio de feriados.');
        });
    }

    function getFeriadosGestion(y) {
      return $http.get(calendarUrl + 'v1/feriados?pais=BO&ano=' + y)
        .then(function (response) {
          return response.data;
        }).catch(function (error) {
          Message.error(error.data || 'No se pudo establecer conexión con el servicio de feriados.');
        });
    }
  }

  function CalFeriadoController($scope, $compile, $timeout, uiCalendarConfig, FeriadosService, $log) {
      var vm = $scope;

      var date = new Date();
      // var d = date.getDate();
      var m = date.getMonth();
      var y = date.getFullYear();
      vm.events = [];

      activate();

      function activate() {
        angular.element('body').on('click', 'button.fc-prev-button', function() {
          prevMounth();
        });

        angular.element('body').on('click', 'button.fc-next-button', function() {
          nextMounth();
        });
      }

      function nextMounth() {
        if (++m == 12) {
          m = 0;
          y++;
        }
        getFechas(m, y);
      }

      function prevMounth() {
        if (--m === 0) {
          m = 12;
          y--;
        }
        getFechas(m, y);
      }

      function getFechas(m, y) {
        return FeriadosService.getFeriados(m, y)
        .then(function(data) {
          if (data && data.feriados) {
            var events = data.feriados;
            for(var i in events) {
              var fecha = events[i].fecha.split('-');
              vm.addEvent({
                title: events[i].nombre,
                start: new Date(fecha[0], fecha[1] -1, parseInt(fecha[2])),
                editable: false
              });
            }
          }
        })
      }

      /* event source that contains custom events on the scope */
      vm.events = [];

      /* alert on eventClick */
      vm.alertOnEventClick = function(date) {
        $log.debug(date.title + ' was clicked ');
      }
      /* alert on Drop */
      vm.alertOnDrop = function(event, delta){
        $log.debug('Event Dropped to make dayDelta ' + delta);
      }
      /* alert on Resize */
      vm.alertOnResize = function(event, delta){
        $log.debug('Event Resized to make dayDelta ' + delta);
      }
      /* add and removes an event source of choice */
      vm.addRemoveEventSource = function(sources,source) {
        var canAdd = 0;
        angular.forEach(sources,function(value, key){
          if(sources[key] === source){
            sources.splice(key,1);
            canAdd = 1;
          }
        })
        if(canAdd === 0){
          sources.push(source);
        }
      }
      /* add custom event*/
      vm.addEvent = function(obj) {
        vm.events.push(obj);
      }
      /* remove event */
      vm.remove = function(index) {
        vm.events.splice(index,1);
      }
      /* Change View */
      vm.changeView = function(view,calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
      }
      /* Change View */
      vm.renderCalender = function(calendar) {
        $timeout(function() {
          if(uiCalendarConfig.calendars[calendar]){
            uiCalendarConfig.calendars[calendar].fullCalendar('render');
            getFechas(m, y);
          }
        }, 1000);
      };

      vm.renderCalender('calFeriados');

      /* Render Tooltip */
      vm.eventRender = function( event, element ) {
        element.attr({'tooltip': event.title,
          'tooltip-append-to-body': true});
        $compile(element)(vm);
      };
      /* config object */
      vm.uiConfig = {
        calendar:{
          lang: 'es',
          editable: true,
          header:{
            left: 'title',
            center: '',
            right: 'today prev,next'
          },
          eventClick: vm.alertOnEventClick,
          eventDrop: vm.alertOnDrop,
          eventResize: vm.alertOnResize,
          eventRender: vm.eventRender
        }
      };

      /* event sources array*/
      vm.eventSources = [vm.events];
    }

  /** @ngInject */
  function FeriadosController(restUrl, $timeout, Modal, Message, DataService) {
    var vm = this;

    vm.title = 'Lista de Feriados personalizados';
    vm.url = restUrl + 'feriados/';
    // vm.fields = ['id', 'username', 'email', 'first_name', 'last_name', 'is_active'];
    // vm.template = 'app/testView/edit.dialog.html';

    vm.sincronizar = sincronizar;

    function sincronizar() {
      Modal.confirm('¿Está seguro de sincronizar los feriados?', function () {
        DataService.get(restUrl + 'revision/feriado/')
        .then(function(data) {
          if (data) {
            Message.success('¡La sincronización fue exitosa!');
            $timeout(function() {
              angular.element('#btn-refresh-crudtable').click();
            }, 1000);
          }
        });
      });
    }
  }
})();
