(function() {
  'use strict';

  angular
    .module('app')
    .controller('DesignacionPermisosController', DesignacionPermisosController);

    /** @ngInject */
    function DesignacionPermisosController(restUrl, Storage, DataService, Modal, Message, $location, $timeout) {
        var vm = this;
        var user = Storage.getUser();
        var rol = user.groups[0];
        var urls = ['', 'designacion_permisos_rrhh/', 'designacion_permisos_jefe/', 'designacion_permisos_funcionario/'];
        var titles = ['', 'Aprobar permisos', 'Aprobar permisos', 'Solicitar permisos'];
        var estados = ['', 'aprobado_rrhh', 'aprobado_jefe', ''];
        var fields = [
            [],
            [
                'id',
                'usuario',
                'tipo_permiso',
                'motivo',
                'comision_lugar',
                'licencia_respaldo',
                // 'particular_numeral_iii',
                // 'particular_numeral_iv',
                'fecha_ini',
                'hora_ini',
                'fecha_fin',
                'hora_fin',
                'unidad_organizacional',
                'jefe',
                'fecha_registro'
            ],
            [
                'id',
                'usuario',
                'tipo_permiso',
                'motivo',
                'comision_lugar',
                'licencia_respaldo',
                // 'particular_numeral_iii',
                // 'particular_numeral_iv',
                'fecha_ini',
                'hora_ini',
                'fecha_fin',
                'hora_fin',
                'unidad_organizacional',
                'fecha_registro'
            ],
            [
                'id',
                'usuario',
                'tipo_permiso',
                'motivo',
                'comision_lugar',
                'licencia_respaldo',
                // 'particular_numeral_iii',
                // 'particular_numeral_iv',
                'fecha_ini',
                'hora_ini',
                'fecha_fin',
                'hora_fin',
                'unidad_organizacional',
                'jefe',
                'fecha_registro'
            ]
        ];
        var permission = [
            {
                print: true
            },
            {
                print: true,
                create: false,
                update: false,
                delete: false
            },
            {
                update: false,
                create: false,
                delete: false
            },
            {
                print: true
            }
        ];

        vm.title = titles[rol];
        vm.url = restUrl + urls[rol];
        vm.column = 2;
        vm.fks = ['usuario', 'jefe', 'tipo_permiso', 'unidad_organizacional'];
        vm.fields = fields[rol];
        vm.permission = permission[rol];
        vm.misPermisos = misPermisos;
        vm.esFuncionario = rol == 3;
        vm.rol = rol;
        vm.idShow = true; // Mostrar el ID
        vm.formly = [
            {
                key: 'id',
                label: 'Correlativo',
                'templateOptions': {
                    label: 'Correlativo'
                }
            },
            {
                key: 'motivo',
                hideExpression: "model.tipo_permiso == 'particular'"
            },
            {
                key: 'usuario',
                defaultValue: (rol == 3 ? user.id : null),
                'templateOptions': {
                    disabled: rol == 3
                }
            },
            {
                key: 'comision_lugar',
                hideExpression: "model.tipo_permiso == 'particular' || model.tipo_permiso == 'licencia'"
            },
            {
                key: 'licencia_respaldo',
                hideExpression: "model.tipo_permiso == 'particular' || model.tipo_permiso == 'comision'"
            },
            {
                key: 'particular_numeral_iii',
                hideExpression: "model.tipo_permiso == 'comision' || model.tipo_permiso == 'licencia'"
            },
            {
                key: 'particular_numeral_iv',
                hideExpression: "model.tipo_permiso == 'comision' || model.tipo_permiso == 'licencia'"
            },
            {
                key: 'fecha_ini',
                defaultValue: new Date()
            },
            {
                key: 'fecha_fin',
                defaultValue: new Date(),
                hideExpression: "model.tipo_permiso == 'olvido_marcar'"
            },
            {
                key: 'hora_ini',
                defaultValue: '08:30',
                hideExpression: "model.tipo_permiso == 'licencia' || model.tipo_permiso == 'olvido_marcar'"
            },
            {
                key: 'hora_fin',
                defaultValue: '19:00',
                hideExpression: "model.tipo_permiso == 'licencia' || model.tipo_permiso == 'olvido_marcar'"
            },
            {
                key: 'tipo_permiso',
                defaultValue: 'comision'
            }
        ];

        if (rol != 3) {
            vm.template = 'app/modules/parameters/designacionPermisos/dialog.permiso.html';
            vm.dialogController = ['$scope', '$mdDialog', 'data', 'fields', 'title', 'add', 'column', 'done', 'Util', DialogController];
            vm.buttons = [
                {
                    tooltip: 'Aprobar permiso',
                    icon: 'checked',
                    onclick: function (event, id) {
                        Modal.confirm('¿Está seguro de aprobar el permiso?', function () {
                            DataService.put(vm.url + id + '/', {estado: estados[rol]})
                            .then(function (response) {
                                if (response) {
                                    Message.success("Permiso Aprobado");
                                    $timeout(function () {
                                        angular.element('#btn-refresh-crudtable').click();
                                    }, 100);
                                }
                            });
                        });
                    }
                },
                {
                    tooltip: 'Rechazar permiso',
                    icon: 'cancel',
                    onclick: function (event, id) {
                        if (rol == 1) {
                            Modal.show({
                                event: event,
                                data: {id : id, url: vm.url},
                                dialogController: ['$scope', '$mdDialog', 'data', 'DataService', 'restUrl', 'Message', DialogRechazoController],
                                templateUrl: 'app/modules/parameters/designacionPermisos/dialog.rechazo.html'
                            });
                        } else {
                            Modal.confirm('¿Está seguro de rechazar el permiso?', function () {
                                DataService.put(vm.url + id + '/', {estado: 'rechazado'})
                                .then(function (response) {
                                    if (response) {
                                        Message.success("Permiso Rechazado");
                                        $timeout(function () {
                                            angular.element('#btn-refresh-crudtable').click();
                                        }, 100);
                                    }
                                });
                            });
                        }
                    }
                }
            ];
        } else {
            vm.template = 'app/modules/parameters/designacionPermisos/dialog.create.permiso.html';
        }

        /* Permisos */
        vm.lang = {
            permisos : {
                comision: 'C',
                particular: 'P',
                licencia: 'L',
                olvido_marcar: 'Olvido de marcado'
            },
            estado: {
                'aprobado_rrhh': 'Aprobado por RRHH',
                'aprobado_jefe': 'Aprobado por Jefe',
                'creado': 'Pendiente de aprobación',
                'rechazado': 'Rechazado'
            }
        };

        activate();

        function activate() {
            getPermisos();
        }

        function getPermisos() {
            DataService.get(vm.url)
            .then(function (response) {
                if (response) {
                    vm.permisos = response;
                }
            });
        }

        function aprobar(event, id) {
            Modal.confirm('¿Está seguro de aprobar el permiso?', function () {
                DataService.put(vm.url + id + '/', {estado: estados[rol]})
                .then(function (response) {
                    if (response) {
                        Message.success("Permiso Aprobado");
                        getPermisos();
                    }
                });
            });
        }

        function rechazar(event, id) {
            Modal.confirm('¿Está seguro de rechazar el permiso?', function () {
                DataService.put(vm.url + id + '/', {estado: 'rechazado'})
                .then(function (response) {
                    if (response) {
                        Message.success("Permiso Rechazado");
                        getPermisos();
                    }
                });
            });
        }

        function verDetalle(index) {
            var $detalle = angular.element(".toggle-detalle[data-toggle='" + index + "']");
            angular.element(".btn-toggle-detalle[data-btn-toggle='" + index + "']").html($detalle.css('display') == 'none' ? 'Ocultar detalle' : 'Ver detalle');
            $detalle.slideToggle();
        }

        vm.aprobar = aprobar;
        vm.rechazar = rechazar;
        vm.verDetalle = verDetalle;

        // vm.eventSave = function (data) {
        //  DataService.pdf(vm.url + 'print/' + data.id + '/')
        //     .then(function (data) {
        //         Modal.show({
        //             data: data,
        //             title: 'Impresión',
        //             templateUrl : 'app/components/crudTable/dialog.pdf.html'
        //         });
        //     });
        // }

        function misPermisos() {
            $location.path('');
            Storage.set('permisos', true);
        }
    }

    function DialogRechazoController($scope, $timeout, $mdDialog, data, DataService, restUrl, Message) {
        var vm = $scope;
        vm.data = data;
        vm.close = $mdDialog.close;
        vm.cancel = $mdDialog.cancel;
        vm.save = save;

        function save() {
            DataService.put(vm.data.url + vm.data.id + '/', {observacion_rrhh: vm.data.observacion_rrhh, estado: 'rechazado'})
            .then(function (response) {
                if (response) {
                    Message.success("Permiso Rechazado");
                    $mdDialog.hide();
                    $timeout(function () {
                        angular.element('#btn-refresh-crudtable').click();
                    }, 100);
                }
            });
        }
    }


    function DialogController($scope, $mdDialog, data, fields, title, add, column, done, Util) {
        var vm = $scope;

        vm.data = data;
        vm.fields = fields;
        vm.title = title;
        vm.add = add;
        vm.column = column;
        vm.querySearch = querySearch;
        vm.save = save;

        activate();

        function activate() {
            vm.funcionarios = getFuncionarios();
            if (vm.data.usuario) {
                vm.searchText = Util.getFkData(vm.fields, 'usuario', vm.data.usuario);
            }
        }

        function getFuncionarios() {
            var users = vm.fields[1].templateOptions.options.map(function (el) {
                return {
                    value: el.value,
                    display: el.name
                };
            });

            return users;
        }

        function querySearch (query) {
            return query ? vm.funcionarios.filter( createFilterFor(query) ) : vm.funcionarios;
        }

        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(state) {
                return (state.display.indexOf(lowercaseQuery) === 0);
            };
        }

        vm.hide = $mdDialog.hide();
        vm.cancel = $mdDialog.cancel;

        function save() {
            if (vm.selectedItem || data.usuario) {
                if (vm.selectedItem) {
                    data.usuario = vm.selectedItem.value;
                }
                delete data.autor;
                done('save', $mdDialog);
            }
        }
    }

})();
