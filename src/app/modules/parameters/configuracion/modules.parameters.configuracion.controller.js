(function() {
    'use strict';

    angular
    .module('app')
    .controller('ConfiguracionController', ConfiguracionController);

    /** @ngInject */
    function ConfiguracionController(restUrl) {
        var vm = this;

        vm.title = 'Configuración';
        vm.url = restUrl + 'configuracion_global/';
        vm.fks = ['horario_lunes', 'horario_martes', 'horario_miercoles', 'horario_jueves', 'horario_viernes', 'horario_sabado', 'horario_domingo'];
        vm.permission = {
            create: false,
            delete: false
        }
        vm.column = 2;

        vm.optionSelected = [
            {
                key: 'horario_lunes',
                text: 'Ninguno'
            },
            {
                key: 'horario_martes',
                text: 'Ninguno'
            },
            {
                key: 'horario_miercoles',
                text: 'Ninguno'
            },
            {
                key: 'horario_jueves',
                text: 'Ninguno'
            },
            {
                key: 'horario_viernes',
                text: 'Ninguno'
            },
            {
                key: 'horario_sabado',
                text: 'Ninguno'
            },
            {
                key: 'horario_domingo',
                text: 'Ninguno'
            }
        ];
        // vm.fields = ['id', 'username', 'email', 'first_name', 'last_name', 'is_active']
        // vm.template = 'app/testView/edit.dialog.html'
    }
})();
