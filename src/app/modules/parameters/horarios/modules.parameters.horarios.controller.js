(function() {
  'use strict';

  angular
    .module('app')
    .controller('HorariosController', HorariosController);

  /** @ngInject */
  function HorariosController(restUrl) {
		var vm = this;

		vm.title = 'Lista de Horarios';
		vm.url = restUrl + 'horarios/';
		// vm.fields = ['id', 'username', 'email', 'first_name', 'last_name', 'is_active'];
		// vm.template = 'app/testView/edit.dialog.html';
  }
})();
