(function() {
  'use strict';

  angular
    .module('app')
    .controller('TipoPermisosController', TipoPermisosController);

  /** @ngInject */
  function TipoPermisosController(restUrl) {
		var vm = this;

		vm.title = 'Lista de Tipo permisos';
		vm.url = restUrl + 'tipo_permisos/';
		// vm.fields = ['id', 'username', 'email', 'first_name', 'last_name', 'is_active'];
		// vm.template = 'app/testView/edit.dialog.html';
	}
})();
