(function() {
  'use strict';

  angular
    .module('app')
    .controller('RegistroHorariosController', RegistroHorariosController);

  /** @ngInject */
  function RegistroHorariosController(restUrl) {
    var vm = this;

    vm.title = 'Lista de registros de horarios';
    vm.url = restUrl + 'registro_horarios/';
    vm.fks = ['usuario', 'horario_rango'];
    vm.fields = [
      'id',
      'usuario',
      'fecha',
      'horario_rango',
      'hora_entrada',
      'hora_salida',
      'observacion'
    ];
    vm.order = [
      'usuario',
      'fecha',
      // 'horario_rango',
      'hora_entrada',
      'hora_salida'
      // 'observacion'
    ];
    // vm.template = 'app/testView/edit.dialog.html';
  }
})();
