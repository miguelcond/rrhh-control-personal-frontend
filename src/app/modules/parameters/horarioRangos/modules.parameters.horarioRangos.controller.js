(function() {
  'use strict';

  angular
    .module('app')
    .controller('HorarioRangosController', HorarioRangosController);

  /** @ngInject */
  function HorarioRangosController(restUrl) {
    var vm = this;

    vm.title = 'Lista de Rangos de Horario';
    vm.url = restUrl + 'horario_rangos/';
    vm.fks = ['horario'];
    vm.fields = [
      'id', 
      'horario', 
      'descripcion', 
      'hora_entrada', 
      'rango_entrada_ini', 
      'rango_entrada_fin', 
      'hora_salida', 
      'rango_salida_ini', 
      'rango_salida_fin'
    ];
    // vm.template = 'app/testView/edit.dialog.html';
  }
})();

