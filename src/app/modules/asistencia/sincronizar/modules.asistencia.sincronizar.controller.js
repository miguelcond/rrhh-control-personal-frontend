(function() {
  'use strict';

  angular
    .module('app')
    .controller('SincronizarController', SincronizarController);

  /** @ngInject */
    function SincronizarController(sincronizarUrl, DataService, Message, Datetime, restUrl, Util) {
        var vm = this;

        vm.listEmpty = false;
        vm.fecha = false;
        vm.maxDate = new Date();
        vm.sincronizar = sincronizar;

        activate();

        function activate() {
            getUltimaSincronizacion();
        }

        function sincronizar () {

            var url = sincronizarUrl;
            if (vm.fecha) {
                if (vm.desde) {
                    url += '?fecha=' + Datetime.parseDate(vm.desde);                    
                } else {
                    Message.warning('Debe seleccionar una fecha para la sincronización.');
                    return false;
                }
            }
            DataService.get(url)
            .then(function (data) {
                if (data) {
                    Message.success(data);
                    getUltimaSincronizacion();
                }   
            })
            .catch(function () {
                Message.error('Ocurrió un error en la sincronización, vuelva a intentarlo más tarde');
            });

        }

        function getUltimaSincronizacion() {
            DataService.get(restUrl + 'registro_horarios_revision_ultimo/')
            .then(function (data) {
                if (data.results) {
                    vm.ultima = Util.formatDate(data.results[0].fecha);
                }
            });
        }

        // function now() {
        //  var date = new Date();
        //  var d = date.getDate();
        //  var m = date.getMonth() + 1;
        //  var y = date.getFullYear();

        //  return [y, (m.toString().length == 1 ? '0': '')+ m, d].join('-');
        // }

    }
})();