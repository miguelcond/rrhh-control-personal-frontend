(function() {
  'use strict';

  angular
    .module('app')
    .controller('ListaConsolidadoController', ListaConsolidadoController);

  /** @ngInject */
  function ListaConsolidadoController(restUrl, DataService, Message, Datetime, reportUrl, $window) {
		var vm = this;

		vm.desde = Datetime.getPrevDateMonth(21);
        vm.hasta = new Date();
		vm.tableEmpty = false;
		vm.consultar = consultar;
		vm.descargar = descargar;
		vm.nuevo = nuevo;
		vm.querySearch = querySearch;

		vm.tipos = [
			{
				value: 'xls',
				text: 'Archivo Excel (.xls)',
				mime: 'application/vnd.ms-excel;charset=charset=utf-8'
			},
			{
				value: 'csv',
				text: 'Archivo CSV (.csv)',
				mime: 'text/csv;charset=charset=utf-8'
			},
			{
				value: 'ods',
				text: 'Archivo LibreCalc(.ods)',
				mime: 'application/vnd.oasis.opendocument.spreadsheet;charset=charset=utf-8'
			}
		];

		/**
		* Search for users... use $timeout to simulate
		* remote dataservice call.
		*/
		function querySearch (query) {
			return query ? vm.funcionarios.filter( createFilterFor(query) ) : vm.funcionarios;
		}

		/**
		* Create filter function for a query string
		*/
		function createFilterFor(query) {
			var lowercaseQuery = angular.lowercase(query);
			return function filterFn(state) {
				return (state.value.indexOf(lowercaseQuery) === 0);
			};
		}

		activate();

		function activate() {
			getUsers();
		}

		function getUsers() {
			var users = []
			DataService.list(restUrl + 'todos_usuarios/')
			.then(function(data) {
				if (data) {
					users.push({
						value: '',
						display: 'Todos los funcionarios'
					});
					for (var i in data) {
						users.push({
							value: data[i].username,
							display: data[i].first_name + ' ' + data[i].last_name
						});
					}
					vm.funcionarios = users;
				}
			})
		}

		function consultar() {
			var url = reportUrl + 'reporte/marcaciones/consolidado/?fecha_ini=' 
					+ Datetime.parseDate(vm.desde) + '&fecha_fin=' 
					+ Datetime.parseDate(vm.hasta) + '&tipo=json';

			DataService.list(url)
			.then(function (data) {
				if (data && data.length > 1) {
					vm.headers = data[0];
					vm.tableEmpty = true;
					vm.report = data.splice(1, data.length);
				} else {
					Message.error('No existen registros para mostrar');
				}
			});
		}

		function descargar() {
			var url = reportUrl + 'reporte/marcaciones/consolidado/?fecha_ini=' 
							+ Datetime.parseDate(vm.desde) + '&fecha_fin=' 
							+ Datetime.parseDate(vm.hasta) + (vm.selectedItem ? '&uid=' + vm.selectedItem.value : '') + '&tipo=' + (vm.tipo || 'ods');

			$window.location = url;

			// var tipo = vm.tipo || 'ods';
			// var filename = 'reporte-consolidado.' + tipo;

			// DataService.file(url, {}, getMime(vm.tipos, tipo), filename)
			// .then(function (response) {
			// 	// console.log(response);
			// 	// $window.location = response + '.' + vm.tipo;
			// });
		}

		function nuevo() {
			vm.tableEmpty = false;
		}

	}
})();
