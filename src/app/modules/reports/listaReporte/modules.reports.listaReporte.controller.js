(function() {
  'use strict';

  angular
    .module('app')
    .controller('ListaReporteController', ListaReporteController);

    /** @ngInject */
	function ListaReporteController(restUrl, DataService, Message, Datetime, reportUrl, $window) {
		var vm = this;

		vm.desde = Datetime.getPrevDateMonth(21);
        vm.hasta = new Date();        
		vm.tableEmpty = false;
		vm.consultar = consultar;
		vm.descargar = descargar;
		vm.nuevo = nuevo;
		vm.querySearch = querySearch;

		vm.tipos = [
			{
				value: 'xls',
				text: 'Archivo Excel (.xls)'
			},
			{
				value: 'csv',
				text: 'Archivo CSV (.csv)'
			},
			{
				value: 'ods',
				text: 'Archivo LibreCalc(.ods)'
			}
		];

		/**
		* Search for users... use $timeout to simulate
		* remote dataservice call.
		*/
		function querySearch (query) {
			return query ? vm.funcionarios.filter( createFilterFor(query) ) : vm.funcionarios;
		}

		/**
		* Create filter function for a query string
		*/
		function createFilterFor(query) {
			var lowercaseQuery = angular.lowercase(query);
			return function filterFn(state) {
				return (state.display.toLowerCase().indexOf(lowercaseQuery) !== -1);
			};
		}

		activate();

		function activate() {
			getUsers();
		}

		function getUsers() {
			var users = []
			DataService.list(restUrl + 'todos_usuarios/')
			.then(function(data) {
				if (data) {
					users.push({
						value: '',
						display: 'Todos los funcionarios'
					});
					for (var i in data) {
						if (data[i].habilitado_marcar) {
							users.push({
								value: data[i].username,
								display: data[i].username + ' - ' +data[i].first_name + ' ' + data[i].last_name
							});
						}
					}
					vm.funcionarios = users;
				}
			})
		}

		function consultar() {
			var url = reportUrl + 'reporte/marcaciones/todos/?fecha_ini=' 
							+ Datetime.parseDate(vm.desde) + '&fecha_fin=' 
							+ Datetime.parseDate(vm.hasta) + (vm.selectedItem ? '&uid=' + vm.selectedItem.value : '') + '&tipo=json';

			DataService.list(url)
			.then(function (data) {
				if (data && data.length > 1) {
					vm.headers = data[0];
					vm.tableEmpty = true;
					vm.report = data.splice(1, data.length);
				} else {
					Message.error('No existen registros para mostrar');
				}
			});
		}

		function descargar() {
			var url = reportUrl + 'reporte/marcaciones/todos/?fecha_ini=' 
							+ Datetime.parseDate(vm.desde) + '&fecha_fin=' 
							+ Datetime.parseDate(vm.hasta) + (vm.selectedItem ? '&uid=' + vm.selectedItem.value : '') + '&tipo=' + vm.tipo;

			$window.location = url;
		}

		function nuevo() {
			vm.tableEmpty = false;
		}

	}
})();
