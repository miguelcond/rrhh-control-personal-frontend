(function() {
    'use strict';

    angular
    .module('app')
    .controller('SugerenciaController', SugerenciaController);

    /** @ngInject */
    function SugerenciaController(restUrl) {
        var vm = this;

        vm.title = 'Lista de Sugerencias';
        vm.url = restUrl + 'sugerencias/';

        vm.fields = [
            'id',
            'sugerencia',
            'fecha_registro'
        ];

        vm.formly = [
            {
                key: 'fecha_registro',
                hideExpression: "true"
            }
        ];
    }
})();
