(function() {
    'use strict';

    angular
    .module('app')
    .controller('UsuariosController', UsuariosController);

    /** @ngInject */
    function UsuariosController(restUrl, Modal, Message, DataService, $timeout) {
        var vm = this;

        vm.title = 'Lista de Usuarios';
        vm.url = restUrl + 'usuarios/';
        vm.fks = ['groups'];
        vm.column = 2;
        vm.fields = [
            'id',
            'username',
            'ci',
            'first_name',
            'last_name',
            'email',
            'fecha_asignacion',
            'cargo',
            'nro_item',
            'cas',
            'unidad_dependencia',
            'groups',
            'habilitado_marcar',
            'is_active'
        ];
        vm.permission = {
            create: false
        };
        vm.formly = [
            {
                key: 'groups',
                'templateOptions': {
                    label: 'Rol de usuario'
                }
            },
            {
                key: 'id',
                'templateOptions': {
                    disabled: true
                }
            }
        ];
        // vm.template = 'app/modules/admin/usuarios/edit.dialog.html';
        
        vm.sincronizar = sincronizar;
        vm.dialogController = ['$scope', '$mdDialog', 'data', 'fields', 'title', 'add', 'column', 'done', 'Util', 'DataService', 'Message', 'restUrl', '$timeout', DialogController];

        function sincronizar() {
            Modal.confirm('¿Está seguro de sincronizar los usuarios de LDAP?', function () {
                DataService.get(restUrl + 'sincronizar/ldap/')
                .then(function(data) {
                    if (data) {
                        Message.success('¡La sincronización fue exitosa!');
                        $timeout(function () {
                            angular.element('#btn-refresh-crudtable').click();                                      
                        }, 500);
                    }
                });
            });
        }

    }

    function DialogController($scope, $mdDialog, data, fields, title, add, column, done, Util, DataService, Message, restUrl, $timeout) {
        var vm = $scope;

        vm.data = data;
        vm.fields = fields;
        vm.title = title;
        vm.add = add;
        vm.column = column;
        vm.answer = answer;
        vm.hide = $mdDialog.hide;
        vm.cancel = $mdDialog.cancel;

        function answer() {
            delete vm.data.password;
            vm.data.groups = [vm.data.groups];
            DataService.patch(restUrl + 'usuarios/', Util.parseSave(vm.data))
            .then(function(data) {
                if (data) {
                    Message.success();
                    $mdDialog.hide();
                    $timeout(function () {
                        angular.element('#btn-refresh-crudtable').click();                                      
                    }, 500);
                }
            });
        }
    }
})();