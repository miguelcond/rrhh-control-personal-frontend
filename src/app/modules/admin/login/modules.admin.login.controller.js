var TIME = 0;
var sessionInterval = null;

(function() {
    'use strict';

    angular
    .module('app')
    .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($auth, $location, Message, SideNavFactory, Storage, $timeout, Util, BreadcrumbFactory, timeSessionExpired, $window, Datetime, $mdDialog, $rootScope) {

        var vm = this;
        vm.login = login;

        activate();

        function activate() {
            angular.element('body').addClass('no-login');

            if (Storage.exist('expired')) {
                Message.warning('Su sesión ha sido cerrada automáticamente después de ' + timeSessionExpired + ' minutos de inactividad.', null, 0);
                Storage.destroy('expired');
            }
        }

        function login () {
            $auth.login({
                username: vm.username,
                password: vm.password
            })
            .then(function(response) {
                var user = response.data.user;
                var menu = response.data.menu;
                var habiles = response.data.vacacion_enable;

                angular.element('body').removeClass('no-login');

                // Set user
                SideNavFactory.setUser(user);
                Storage.setUser(user);

                // Set Menu
                SideNavFactory.setMenu(menu);
                Storage.set('menu', SideNavFactory.getMenu());

                // Meses hábiles
                Storage.set('habiles', habiles);

                var path = Storage.get('path');

                if (path) {
                    var route = path.replace('/', '');

                    // Set breadcrumb
                    var page = Util.getMenuOption(Storage.get('menu'), route);
                    BreadcrumbFactory.setParent(page[0]);
                    BreadcrumbFactory.setCurrent(page[1]);

                    // Select menu option
                    $timeout(function () {
                        var $sidenav = angular.element('#sidenav');
                        $sidenav.find('.md-button-href').removeClass('active');
                        $sidenav.find('.sidenav-sublist').hide();
                        $sidenav.find(".md-button-href[data-url='" + route + "']")
                                .addClass('active').parent().parent().prev().click();
                    }, 1000);

                }

                $rootScope.rrhh = Storage.getUser().groups[0] == 1;

                TIME = timeSessionExpired*60;
                sessionInterval = $window.setInterval(function () {
                    TIME--;
                    if (TIME <= 0) {
                        $window.clearInterval(sessionInterval);
                        logout();
                    }
                }, 1000);

                angular.element('#toast-container-main').find('md-toast').fadeOut();

                // Redirect to dashboard
                $location.path(path && path.indexOf('login') == -1? path : '/');

            })
            .catch(function(error) {
                if (error.status == 400) {
                    Message.error('Su Nombre de usuario y Contraseña no son válidos.');                    
                }
            });
        }

        function logout () {
            $auth.logout()
            .then(function() {
                Storage.set('expired', true);
                Storage.removeUser();
                Storage.remove('menu');
                Storage.remove('path');
                $location.path("login");
                $mdDialog.hide();
            });
        }

    }

})();
