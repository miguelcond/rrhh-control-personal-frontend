(function() {
	'use strict';

	angular
	.module('app')	
	.controller('ProfileController', ProfileController);

	/** @ngInject */
	function ProfileController(SideNavFactory, Datetime) {
		var vm = this;

		vm.getColor = function () {
			return SideNavFactory.userColor;
		}

		vm.getName = function () {
			var user = SideNavFactory.getUser();
			return user.first_name + ' ' + user.last_name;
		}

		vm.getEmail = function () {
			return SideNavFactory.getUser().email;
		}

		vm.getInitial = function () {
			var firstName = SideNavFactory.getUser().first_name;
			return firstName.length ? firstName[0].toUpperCase() : '?';
		}

		vm.getData = function (key) {

			var value = SideNavFactory.getUser()[key];
			if (['last_login', 'date_joined'].indexOf(key) != -1) {
				return Datetime.datetimeLiteral(new Date(value));
			}

			return value;
		}
	}
})();
