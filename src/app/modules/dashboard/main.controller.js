(function() {
    'use strict';

    angular
    .module('app')
    .config(function (ChartJsProvider) {
        // Configure all charts
        ChartJsProvider.setOptions({
            colours: ['#97BBCD', '#DCDCDC', '#46BFBD', '#F7464A', '#FDB45C', '#949FB1', '#4D5360'],
            responsive: true
        });
    })
    .controller('MainController', MainController);

    /** @ngInject */
    function MainController($scope, SideNavFactory, Datetime, reportUrl, DataService, Message, restUrl, Storage, $timeout, Util, Modal, $element, Filter) {

        var vm = this;

        var toleranciaMes = 30*60; // 30min
        var tiempoParticularMes = 2*60*60; // 2h al mes
        var tiempoLicenciaAnio = 16*60*60; //2 días al año
        var tipoPermisos = ['comision', 'particular', 'licencia'];
        var inicioCorte = 26; // Define el dia de inicio de corte para los rangos de fechas

        vm.permiso_usado_mes = 0;
        vm.permiso_restante_mes = 0;
        vm.licencia_usado_mes = 0;
        vm.licencia_restante_year = 0;
        vm.user = Storage.getUser();

        vm.url = restUrl + 'designacion_permisos_funcionario/';
        vm.add = add;
        vm.edit = edit;
        vm.delete = deleteItem;
        vm.print = print;
        vm.desde = Datetime.getPrevDateMonth(inicioCorte);
        vm.hasta = new Date();
        vm.desdeP = Datetime.getPrevDateMonth(inicioCorte);
        vm.hastaP = new Date();
        vm.consultar = consultar;
        vm.getColor = getColor;
        vm.getName = getName;
        vm.getEmail = getEmail;
        vm.getInitial = getInitial;
        vm.verDetalle = verDetalle;
        vm.getClass = getClass;
        vm.getData = getData;
        vm.imprimir = imprimir;
        vm.obtenerPermisos = obtenerPermisos;
        vm.verMarcaciones = verMarcaciones;
        vm.tableEmpty = false;
        vm.maxDate = new Date();
        vm.month = Datetime.format(new Date(), 'MMM');
        vm.lang = {
            permisos : {
                comision: 'C',
                particular: 'P',
                licencia: 'L',
                olvido_marcar: 'Olvido de marcado'
            },
            estado: {
                'aprobado_rrhh': 'Aprobado por RRHH',
                'aprobado_jefe': 'Aprobado por Jefe',
                'creado': 'Pendiente de aprobación',
                'rechazado': 'Rechazado',
                'creado_vacacion': 'Pendiente de aprobación',
                'aprobado_vacacion': 'Aprobado',
                'rechazado_vacacion': 'Rechazado para reprogramación'
            }
        };

        vm.template = 'app/modules/parameters/designacionPermisos/dialog.create.permiso.html';
        vm.column = 2;
        vm.fields1 = [
            'id',
            'usuario',
            'tipo_permiso',
            'motivo',
            'comision_lugar',
            'licencia_respaldo',
            'unidad_organizacional',
            'jefe',
            'fecha_registro'
        ];
        vm.formly = [
            {
                key: 'motivo',
                hideExpression: "model.tipo_permiso == 'particular'"
            },
            {
                key: 'fecha_registro',
                hideExpression: "true"
            },
            {
                key: 'usuario',
                defaultValue: Storage.getUser().id,
                templateOptions: {
                    disabled: true
                }
            },
            {
                key: 'comision_lugar',
                hideExpression: "model.tipo_permiso != 'comision'"
            },
            {
                key: 'licencia_respaldo',
                hideExpression: "model.tipo_permiso != 'licencia'"
            },
            {
                key: 'fecha_ini',
                defaultValue: new Date()
            },
            {
                key: 'fecha_fin',
                defaultValue: new Date(),
                hideExpression: "model.tipo_permiso == 'particular' || model.tipo_permiso == 'olvido_marcar' || (model.tipo_permiso == 'licencia' && (model.rango == 'uno' || model.rango == 'rango'))"
            },
            {
                key: 'hora_ini',
                defaultValue: '08:30',
                hideExpression: "model.tipo_permiso == 'licencia' && (model.rango == 'uno' || model.rango == 'dos')"
            },
            {
                key: 'hora_fin',
                defaultValue: '19:00',
                hideExpression: "model.tipo_permiso == 'olvido_marcar' || (model.tipo_permiso == 'licencia' && (model.rango == 'uno' || model.rango == 'dos'))"
            },
            {
                key: 'tipo_permiso',
                defaultValue: 'comision'
            },
            {
                key: 'estado',
                defaultValue: 'aprobado_rrhh'
            }
        ];

        activate();

        function activate() {
            consultar(true);
            obtenerPermisos(true);

            if (Storage.exist('permisos')) {
                vm.selectedIndex = 2;
                Storage.remove('permisos');
            }

            getFields();
        }

        function getClass(permiso, obs) {
            if (permiso && obs.length && obs != 'No marcó') {
                return 'horario-permiso';
            } else if (obs.length) {
                return obs == 'No marcó' ? 'no-marco' : 'horario-retraso';
            }
            return '';
        }

        function verMarcaciones(event, uid, fecha) {
            fecha = fecha.split('/');
            DataService.get(reportUrl + 'proxy/api/v1/registros_usuario_dia?uid=' + uid + '&fecha=' + [fecha[2], fecha[1], fecha[0]].join('-'))
            .then(function (response) {                
                if (response) {                    
                    Modal.show({
                        event: event,
                        data: response,
                        title: 'Total marcaciones en fecha: ' + fecha.join('/'),
                        templateUrl : 'app/modules/dashboard/dialog.marcacion.html',
                        controller: ['$scope', '$mdDialog', 'data', 'title', DialogMarcacionController]
                    });
                }
            });
        }

        function DialogMarcacionController($scope, $mdDialog, data, title) {
            var vm = $scope;

            vm.title = title;
            vm.data = data;
            vm.hide = $mdDialog.hide;
            vm.cancel = $mdDialog.cancel;
        }

        function loadChart(data) {
            var labels = [];

            data[0].map(function (item, index) {
                labels.push(item.fecha);
                data[0][index] = parseInt(item.valor/60);
            });
            data[1].map(function (item, index) {
                data[1][index] = parseInt(item.valor/60);
            });

            vm.labels = labels;
            vm.series = ['Atrasos 1er. Periodo [minutos/día]', 'Atrasos 2do. Periodo [minutos/día]'];
            vm.data = data;
        }

        function getColor() {
            return SideNavFactory.userColor;
        }

        function getName() {
            var user = SideNavFactory.getUser();
            return user.first_name + ' ' + user.last_name;
        }

        function getEmail() {
            return SideNavFactory.getUser().email;
        }

        function getInitial() {
            var firstName = SideNavFactory.getUser().first_name;
            return firstName.length ? firstName[0].toUpperCase() : '?';
        }

        function getData(key) {

            var value = SideNavFactory.getUser()[key];
            if (['last_login', 'date_joined'].indexOf(key) != -1 ) {
                return Datetime.datetimeLiteral(new Date(value));
            } else if (['fecha_asignacion'].indexOf(key) != -1 && value) {
                var fecha = value.split('-');
                return Datetime.dateLiteral(new Date(parseInt(fecha[0]), parseInt(fecha[1]) - 1, parseInt(fecha[2])));
            }

            return value;
        }

        function consultar(now) {
            var url = reportUrl + 'reporte/marcaciones/individual/?fecha_ini=' +
                      Datetime.parseDate(vm.desde) + '&fecha_fin=' +
                      Datetime.parseDate(vm.hasta) + '&tipo=json';

            DataService.list(url)
            .then(function (data) {
                if (data && data.length > 1) {
                    data.shift();
                    var report = getDataReport(data);
                    vm.report = report.items;
                    if (now) {
                        vm.total_atrasos_mes = Datetime.timeLiteral(report.totalAtrasos) || 0;
                        vm.total_restante_mes = report.totalAtrasos <= toleranciaMes ? Datetime.timeLiteral(toleranciaMes - report.totalAtrasos) : '0m';
                        loadChart(report.data);
                    }
                    var total = Datetime.timeLiteral(report.totalAtrasos);
                    vm.total_atrasos = total.length ? total : 0;
                    vm.tableEmpty = true;
                } else {
                    Message.error('No existen registros para mostrar');
                }
            });
        }

        function mergeData(data) {
            var items = {};

            for (var i in data) {
                if (items[data[i][3]]) {
                    var item = null;
                    if (tipoPermisos.indexOf(data[i][4].toLowerCase()) != -1) {
                        item = items[data[i][3]];
                        if (item.permisos) {
                            item.permisos.push(data[i]);
                        } else {
                            item.permisos = [data[i]];
                        }
                    } else {
                        items[data[i][3]].second = data[i];
                        // if (data[i][4] == 'olvido_marcar') {
                        //     item = items[data[i][3]];
                        //     if (item.olvidos) {
                        //         item.olvidos.push(data[i]);
                        //     } else {
                        //         item.olvidos = [data[i]];
                        //     }
                        // } else {
                        //     items[data[i][3]].second = data[i];
                        // }
                    }
                } else {
                    items[data[i][3]] = {
                        first: data[i]
                    };
                }
            }

            return items;
        }

        function getDataReport(data) {
            data = mergeData(data);
            var items = [];
            var totalAtrasos = 0;
            var dataFirst = [];
            var dataSecond = [];

            for (var i in data) {
                var item = data[i];
                var t = item.first[15]*60;
                var retraso1 = 0, retraso2 = 0;

                if (item.first[12].length) {
                    retraso1 = Datetime.getSeconds(item.first[12]);
                    dataFirst.push({
                        fecha: item.first[3],
                        valor: retraso1
                    });
                } else {
                    dataFirst.push({
                        fecha: item.first[3],
                        valor: 0
                    });
                }

                if (item.second && item.second[12].length) {
                    retraso2 = Datetime.getSeconds(item.second[12]);
                    dataSecond.push({
                        fecha: item.first[3],
                        valor: retraso2
                    });
                } else {
                    dataSecond.push({
                        fecha: item.first[3],
                        valor: 0
                    });
                }

                totalAtrasos += retraso1 + retraso2;

                var classHorario = '';
                if (item.first[5] == 'X') {
                    classHorario = 'horario-feriado';
                } else if (item.first[4] == 'SIN HORARIO') {
                    classHorario = 'sin-horario';
                } else if (item.first[4] == 'FALTO') {
                    classHorario = 'horario-retraso';
                }

                var entrada1_obs = '';
                if (item.first[5] != 'X') {
                    if (item.first[8].length) {
                        entrada1_obs = Datetime.timeLiteral(retraso1, 'Retraso<br>');
                        if (item.permisos) {
                            entrada1_obs = getPermisos(item.permisos, item.first[6], item.first[7]) + '<div class="asistencia-retraso">' + entrada1_obs + '</div>';
                        }
                    } else {
                        if (item.first[14] == "CON PERMISO" || item.first[14] == "PERMISO") {
                            entrada1_obs = getPermisos(item.permisos, item.first[6], item.first[7]);
                        } else {
                            entrada1_obs = item.second ? getOlvidos(item.olvidos, item.second[6], item.second[7]) : '';
                        }
                    }
                }
                var salida1_obs = '';
                if (item.first[5] != 'X') {
                    if (item.first[9].length) {
                        salida1_obs = Datetime.timeLiteral(item.first[13], 'Adelanto<br>');
                        if (item.permisos) {
                            salida1_obs = getPermisos(item.permisos, item.first[6], item.first[7]) + '<div class="asistencia-retraso">' + salida1_obs + '</div>';
                        }
                    } else {
                        if (item.first[14] == "CON PERMISO" || item.first[14] == "PERMISO") {
                            salida1_obs = getPermisos(item.permisos, item.first[6], item.first[7]);
                        } else {
                            salida1_obs = item.second ? getOlvidos(item.olvidos, item.second[6], item.second[7]) : '';                        
                        }
                    }
                }

                var entrada2_obs = '';
                if (item.first[5] != 'X') {
                    if (item.second) {
                        if (item.second[8].length) {
                            entrada2_obs = Datetime.timeLiteral(retraso2, 'Retraso<br>');
                            if (item.permisos) {
                                entrada2_obs = getPermisos(item.permisos, item.second[6], item.second[7]) + '<div class="asistencia-retraso">' + entrada2_obs + '</div>';
                            }
                        } else {
                            if (item.second[14] == "CON PERMISO" || item.second[14] == "PERMISO") {
                                entrada2_obs = getPermisos(item.permisos, item.second[6], item.second[7]);
                            } else {
                                entrada2_obs = getOlvidos(item.olvidos, item.second[6], item.second[7]);
                            }
                        }
                    } else {
                        entrada2_obs = '';
                    }
                }
                var salida2_obs = '';
                if (item.first[5] != 'X') {
                    if (item.second) {
                        if (item.second[9].length) {
                            salida2_obs = Datetime.timeLiteral(item.second[13], 'Adelanto<br>');
                            if (item.permisos) {
                                salida2_obs = getPermisos(item.permisos, item.second[6], item.second[7]) + '<div class="asistencia-retraso">' + salida2_obs + '</div>';
                            }
                        } else {
                            if (item.second[14] == "CON PERMISO" || item.second[14] == "PERMISO") {
                                salida2_obs = getPermisos(item.permisos, item.second[6], item.second[7]);
                            } else {
                                salida2_obs = getOlvidos(item.olvidos, item.second[6], item.second[7]);                            
                            }
                        }
                    } else {
                        salida2_obs = '';
                    }
                }

                items.push({
                    dia: item.first[2],
                    fecha: item.first[3],
                    horario1: item.first[4],
                    entrada1: item.first[8],
                    salida1: item.first[9],
                    horario2: item.second ? item.second[4] : '',
                    entrada2: item.second ? item.second[8] : '',
                    salida2: item.second ? item.second[9] : '',
                    entrada1_obs: entrada1_obs,
                    salida1_obs: salida1_obs,
                    entrada2_obs: entrada2_obs,
                    salida2_obs: salida2_obs,
                    sin_horario: classHorario,
                    retraso: retraso1 + retraso2 > 0,
                    permiso: !!item.permisos || !!item.olvidos
                });
            }
            return {
                items: items,
                totalAtrasos: totalAtrasos,
                data: [dataFirst, dataSecond]
            };
        }

        function getPermisos(array, ini, fin) {
            var permisos = [], text, estado, i;
            if (ini.length && fin.length) {
                ini = Datetime.getSeconds(ini);
                fin = Datetime.getSeconds(fin);
                for (i in array) {
                    var iniC = Datetime.getSeconds(array[i][8]);
                    var finC = Datetime.getSeconds(array[i][9]);
                    estado = array[i][4].toLowerCase();
                    if ((iniC >= ini && iniC <= fin) || (finC >= ini && finC <= fin)) {
                        text = '<strong>' + vm.lang.permisos[estado] + '</strong>: ' + array[i][8] + ' a ' + array[i][9];
                        permisos.push(text);
                    }
                }
            } else {
                for (i in array) {
                    estado = array[i][4].toLowerCase();
                    text = '<strong>' + vm.lang.permisos[estado] + '</strong>: ' + array[i][8] + ' a ' + array[i][9];
                    permisos.push(text);
                }
            }
            if (permisos.length) {
                return permisos.join('<br />');
            }
            return '';
        }

        function getOlvidos(array, ini, fin) {
            if (array && array.length) {
                ini = Datetime.getSeconds(ini);
                fin = Datetime.getSeconds(fin);
                var olvidos = [];
                for (var i in array) {
                    var iniC = Datetime.getSeconds(array[i][8]);
                    var finC = Datetime.getSeconds(array[i][9]);
                    if (iniC >= ini && iniC <= fin) {
                        var text = '<strong>' + vm.lang.permisos[array[i][4]] + '</strong>: ' + array[i][8];
                        olvidos.push(text);
                    }
                }
                if (olvidos.length) {
                    return olvidos.join('<br />');
                }
            }
            return 'No marcó';
        }

        vm.particular_save = true;

        function obtenerPermisos(now) {
            var url = restUrl + 'historico_permisos_por_usuario/?fecha_ini=' + Datetime.parseDate(vm.desdeP) +
                      '&fecha_fin=' + Datetime.parseDate(vm.hastaP);
            DataService.list(url)
            .then(function (data) {
                if (data) {
                    vm.permisos = data;
                    if (now) {
                        vm.total_permisos_mes = data.count;
                        var datos = getDataPermisos(data);

                        var mes = tiempoParticularMes - datos.particular;
                        vm.particular_mes = mes >= 0 ? mes : 0;
                        vm.particular_usado_mes = Datetime.timeLiteral(datos.particular) || 0;
                        vm.particular_restante_mes = Datetime.timeLiteral(vm.particular_mes);
                        vm.particular_save = !!vm.particular_restante_mes;

                        var year = tiempoLicenciaAnio - datos.licencia;
                        vm.licencia_year = year >= 0 ? year : 0;
                        vm.licencia_usado_mes = Datetime.timeLiteral(datos.licencia) || 0;
                        vm.licencia_restante_year = Datetime.timeLiteral(vm.licencia_year) || 0;
                        vm.licencia_save = !!vm.licencia_restante_year;

                        vm.total_comision = datos.total_comision;
                        vm.total_licencia = datos.total_licencia;
                        vm.total_particular = datos.total_particular;
                    }
                }
            });
        }

        function getDataPermisos(data) {
            var datos = {
                total_comision: 0,
                total_licencia: 0,
                total_particular: 0,
                particular: 0,
                licencia: 0
            };
            for (var i in data) {
                if (data[i].tipo_permiso == 'particular' && (data[i].estado == 'aprobado_rrhh' || data[i].estado == 'aprobado_jefe')) {
                    datos.particular += Datetime.getSeconds(data[i].hora_fin) - Datetime.getSeconds(data[i].hora_ini);
                }
                if (data[i].tipo_permiso == 'licencia' && (data[i].estado == 'aprobado_rrhh' || data[i].estado == 'aprobado_jefe')) {
                    if (data[i].hora_ini == '08:30:00' && data[i].hora_fin == '19:00:00') {
                        datos.licencia += 8*60*60; // 8 horas en segundos
                    } else {
                        var hora_ini = Datetime.getSeconds(data[i].hora_ini);
                        var hora_fin = Datetime.getSeconds(data[i].hora_fin);
                        datos.licencia += (hora_fin - hora_ini);
    
                        // 8:30 = 30600
                        // 12:00 = 43200
                        // 12:00 - 8:30 = 12600 (Total primer periodo en segundos)
                        // 14:30 - 12:00 = 9000 (Descanso medio día en segundos)
                        if (hora_ini >= 30600 && hora_fin <= 43200 && hora_fin - hora_ini > 12600 + 9000) {
                            datos.licencia -= 9000;
                        }
                    }
                }
                if (data[i].estado == 'aprobado_rrhh' || data[i].estado == 'aprobado_jefe') {
                    datos['total_' + data[i].tipo_permiso]++;
                }
            }
            return datos;
        }

        function verDetalle(index) {
            var $detalle = angular.element(".toggle-detalle[data-toggle='" + index + "']");
            angular.element(".btn-toggle-detalle[data-btn-toggle='" + index + "']").html($detalle.css('display') == 'none' ? 'Ocultar detalle' : 'Ver detalle');
            $detalle.slideToggle();
        }

        function imprimir() {
            angular.element('#hora-reporte').html(Datetime.datetimeLiteral(new Date()));
            Util.print(angular.element('#mi-asistencia').html());
        }

        // Gestión de permisos
        function add (event, id) {
            var add = typeof id == 'undefined';
            if (!add) {
                getItem(event, add, id);
            } else {
                openDialog(event, add, {});
            }
        }

        function edit (event, item) {
            add(event, item);
        }

        function deleteItem (event, id) {
            Modal.confirm('Se eliminará el registro. ¿Está seguro?.', function () {
                DataService.get(vm.url, id)
                .then(function(data) {
                    if (data && data.id) {
                        data.estado = 'eliminado';
                        DataService.save(vm.url, Util.parseSave(data))
                        .then(function(data) {
                            if (data) {
                                Message.success();
                                obtenerPermisos();
                            }
                        });
                    }
                });
            }, null, event);
        }

        function getItem(event, add, id) {
            DataService.get(vm.url, id)
            .then(function(data) {
                if (data && data.id) {
                    openDialog(event, add, Util.filterItem(data));
                }
            });
        }

        function openDialog(event, add, data) {
            var ini, fin;
            if (typeof data.id == 'undefined') {
                data.tipo_permiso = 'comision';
                data.fecha_ini = new Date();
                data.fecha_fin = new Date();

                ini = new Date();
                ini.setHours(8);
                ini.setMinutes(30);
                data.hora_ini = ini;

                fin = new Date();
                fin.setHours(19);
                fin.setMinutes(0);
                data.hora_fin = fin;
                data.rango = 'rango';
            } else {
                data.rango = (data.hora_ini == "08:30" && data.hora_fin == "19:00") || (typeof data.hora_ini == 'undefined' && typeof data.hora_fin == 'undefined') ? 'uno' : 'rango';
                data.hora_ini = data.hora_ini.split(':');
                ini = new Date();
                ini.setHours(data.hora_ini[0]);
                ini.setMinutes(data.hora_ini[1]);
                data.hora_ini = ini;

                data.hora_fin = data.hora_fin.split(':');
                fin = new Date();
                fin.setHours(data.hora_fin[0]);
                fin.setMinutes(data.hora_fin[1]);
                data.hora_fin = fin;
            }
            data.particular_save = vm.particular_save;
            data.licencia_save = vm.licencia_save;
            data.particular_restante_mes = vm.particular_restante_mes;
            data.licencia_restante_year = vm.licencia_restante_year;
            var config = {
                event: event,
                data: data,
                title: add ? 'Agregar Permiso' : 'Editar Permiso',
                add: add,
                fields: vm.fieldsData,
                column: vm.column || 1,
                templateUrl: vm.template || 'app/components/crudTable/dialog.crudTable.html',
                controller : ['$scope', '$mdDialog', 'data', 'fields', 'title', 'add', 'column', 'done', 'Util', DialogControllerPermisos],
                done: function (answer, $mdDialog) {
                    if (answer == 'save') {
                        var item = {
                            "id": data.id,
                            "jefe": data.jefe,
                            "estado": data.estado,
                            "tipo_permiso": data.tipo_permiso,
                            "motivo": data.motivo,
                            "comision_lugar": data.comision_lugar,
                            "licencia_respaldo": data.licencia_respaldo,
                            "fecha_ini": data.fecha_ini,
                            "fecha_fin": data.fecha_fin,
                            "hora_ini": Datetime.format(data.hora_ini, 'HH:mm'),
                            "hora_fin": Datetime.format(data.hora_fin, 'HH:mm'),
                            "fecha_registro": data.fecha_registro,
                            "observacion_rrhh": data.observacion_rrhh,
                            "unidad_organizacional":data.unidad_organizacional,
                            "usuario": Storage.getUser().id
                        };
                        var message = validateTime(item, data.particular_restante_mes);
                        if (message) {
                            Message.warning(message);
                        } else {
                            item.observacion_rrhh = 'Ninguna';

                            if (item.tipo_permiso == 'olvido_marcar') {
                                item.fecha_fin = item.fecha_ini;
                            }

                            if (item.tipo_permiso == 'licencia') {
                                if (data.rango == 'uno') {
                                    if (8*60*60 <= vm.licencia_year) {
                                        item.fecha_fin = item.fecha_ini;
                                        item.hora_ini = '08:30';
                                        item.hora_fin = '19:00';
                                    } else {
                                        Message.warning('Ya no tiene tiempo disponible o seleccione un rango de hora inferior');
                                        return false;
                                    }
                                } else if (data.rango == 'dos') {
                                    if (16*60*60 > vm.licencia_year) {
                                        Message.warning('Ya no tiene tiempo disponible o seleccione un rango de hora inferior');
                                        return false;
                                    }
                                } else if (data.rango == 'rango') {
                                    message = validateTimeLicencia(item, data.licencia_restante_year);
                                    if (message) {
                                        Message.warning(message);
                                        return false;
                                    } else {
                                        item.fecha_fin = item.fecha_ini;
                                    }
                                }
                            }

                            if (item.tipo_permiso == 'particular') {
                                item.fecha_fin = item.fecha_ini;
                            }

                            DataService.save(vm.url, Util.parseSave(item))
                            .then(function(data) {
                                if (data) {
                                    Message.success();
                                    $mdDialog.hide();
                                    obtenerPermisos();
                                }
                            });
                        }
                    }
                    return true;
                }
            };

            Modal.show(config);
        }        

        function DialogControllerPermisos($scope, $mdDialog, data, fields, title, add, column, done, Util) {
            var vm = $scope;

            vm.fields = fields;
            vm.title = title;
            vm.add = add;
            vm.keys = Util.getKeys(fields);
            vm.column = column;
            vm.hide = $mdDialog.hide;
            vm.cancel = $mdDialog.cancel;

            activate();

            function activate() {
                getJefes();
            }

            function getJefes() {
                var users = [];
                DataService.list(restUrl + 'todos_jefes/')
                .then(function(response) {
                    if (response) {
                        for (var i in response) {
                            if (response[i].habilitado_marcar && response[i].groups[0] == 2) {
                                users.push({
                                    value: response[i].id,
                                    name: response[i].username + ' - ' + response[i].first_name + ' ' + response[i].last_name
                                });
                            }
                        }
                        vm.jefes = users;
                        vm.data = data;
                    }
                });
            }

            vm.answer = function(answer) {
                done(answer, $mdDialog);
            };
        }

        function validateTime(item, particular_restante_mes) {
            if (item.tipo_permiso == 'particular') {
                var time = Datetime.getSeconds(item.hora_fin) - Datetime.getSeconds(item.hora_ini);
                if (time <= 0) {
                    return 'La hora de inicio no puede ser mayor a la hora final';
                } else {
                    if (time > vm.particular_mes) {
                        return 'Usted quiere pedir ' + Datetime.timeLiteral(time) + ' pero solo tiene ' + particular_restante_mes + ' disponible para pedir permiso';
                    }
                }
            }
            return null;
        }

        function validateTimeLicencia(item, licencia_restante_year) {
            var time = Datetime.getSeconds(item.hora_fin) - Datetime.getSeconds(item.hora_ini);
            if (time <= 0) {
                return 'La hora de inicio no puede ser mayor a la hora final';
            } else {
                var hora = vm.licencia_year > 8*60*60 ? 8*60*60 : vm.licencia_year;
                if (time > hora) {
                    return 'Usted quiere pedir ' + Datetime.timeLiteral(time) + ' pero solo tiene ' + licencia_restante_year + ' disponible para pedir permiso';
                }
            }
            return null;
        }

        function getFields() {
            DataService.fields(vm.url)
            .then(function(data) {
                if (data) {
                    vm.fieldsData = Util.filterFields(DjangoRestFormly.toFormly(data.actions.POST), vm.fields1);
                    vm.fieldsData = Util.addPropertiesFormly(vm.fieldsData, vm.formly);
                }
            });
        }

        function print(event, id) {
            DataService.pdf(vm.url + 'print/' + id + '/')
            .then(function (data) {
                Modal.show({
                    event: event,
                    data: data,
                    title: 'Impresión',
                    templateUrl : 'app/components/crudTable/dialog.pdf.html',
                    controller: ['$scope', '$mdDialog', 'data', 'title', PdfController]
                });
            });
        }

        function PdfController($scope, $mdDialog, data, title) {
            var vm = $scope;

            vm.title = title;
            vm.data = data;
            vm.hide = $mdDialog.hide;
            vm.cancel = $mdDialog.cancel;
        }

        // Vacaciones
        
        getOptionsVacacion();

        function getOptionsVacacion() {
            DataService.options(restUrl + 'designacion_vacacion_funcionario/')
            .then(function (response) {
                if (response) {
                    vm.optionsVacacion = DjangoRestFormly.toFormly(response.actions.POST);
                    obtenerVacaciones();
                }
            });
        }

        function obtenerVacaciones() {
            if (Filter.empty(vm.user.fecha_asignacion)) {
                vm.sin_fecha = true;
            }

            getDiasDisponibles();

            DataService.get(restUrl + 'designacion_vacacion_funcionario/')
            .then(function (response) {
                if (response) {
                    var vacaciones = response;
                    var items = [];

                    for (var i in vacaciones) {
                        vacaciones[i].unidad_organizacional = Util.getFkData(vm.optionsVacacion , 'unidad_organizacional', vacaciones[i].unidad_organizacional);
                        vacaciones[i].jefe = Util.getFkData(vm.optionsVacacion, 'jefe', vacaciones[i].jefe);
                        items.push(vacaciones[i]);
                    }

                    vm.vacaciones = items;
                }
            });
        }

        function getDiasDisponibles() {
            DataService.get(restUrl + 'dias_vacaciones/')
            .then(function (response) {
                if (response) {
                    vm.dias_disponibles = response.vacacion;
                }
            });            
        }       
        
        vm.gestion_vacacion = Util.getGestion();
        vm.agregarVacacion = agregarVacacion;
        vm.editarVacacion = editarVacacion;
        vm.eliminarVacacion = eliminarVacacion;
        vm.imprimirCalendario = imprimirCalendario;

        // Gestión de permisos
        function agregarVacacion (event, id) {
            var add = typeof id == 'undefined';
            if (!add) {
                getVacacion(event, add, id);
            } else {
                openDialogVacacion(event, add, {});
            }
        }

        function editarVacacion (event, item) {
            agregarVacacion(event, item);
        }

        function eliminarVacacion (event, id) {
            Modal.confirm('Se eliminará el registro. ¿Está seguro?.', function () {
                DataService.get(restUrl + 'designacion_vacacion_funcionario/', id)
                .then(function(data) {
                    if (data && data.id) {
                        data.estado = 'eliminado';
                        DataService.save(restUrl + 'designacion_vacacion_funcionario/', Util.parseSave(data))
                        .then(function(data) {
                            if (data) {
                                Message.success();
                                obtenerVacaciones();
                            }
                        });
                    }
                });
            }, null, event);
        }

        function getVacacion(event, add, id) {
            DataService.get(restUrl + 'designacion_vacacion_funcionario/', id)
            .then(function(data) {
                if (data && data.id) {
                    openDialogVacacion(event, add, Util.filterItem(data));
                }
            });
        }

        function openDialogVacacion(event, add, data) {
            var ini, fin;            
            data.fecha_asignacion = vm.user.fecha_asignacion;
            data.dias_disponibles = vm.dias_disponibles;
            data.gestion_vacacion = vm.gestion_vacacion;

            if (typeof data.id == 'undefined') {
                data.tipo_vacacion = 'completo';

                ini = new Date();
                ini.setHours(8);
                ini.setMinutes(30);
                data.hora_ini = ini;

                fin = new Date();
                fin.setHours(19);
                fin.setMinutes(0);
                data.hora_fin = fin;
            }

            var config = {
                event: event,
                data: data,
                title: add ? 'Agregar Fecha de vacación' : 'Editar Fecha de vacación',
                add: add,
                fields: vm.fieldsData,
                column: vm.column || 1,
                templateUrl: 'app/modules/dashboard/dialog.vacacion.html',
                controller : ['$scope', '$mdDialog', 'data', 'fields', 'title', 'add', 'column', 'done', 'Util', 'FeriadosService', 'restUrl', 'Datetime', DialogControllerVacacion],
                done: function (answer, $mdDialog) {
                    if (answer == 'save') {
                        var item = {
                            'id': data.id,
                            'usuario': vm.user.id,
                            'jefe': data.jefe,
                            'estado': 'creado',
                            'tipo_vacacion': data.tipo_vacacion,
                            'fecha_ini': data.fecha_ini,
                            'fecha_fin': data.fecha_fin,
                            'hora_ini': Datetime.format(data.hora_ini, 'HH:mm'),
                            'hora_fin': Datetime.format(data.hora_fin, 'HH:mm'),
                            'fecha_registro': data.fecha_registro,
                            'unidad_organizacional': data.unidad_organizacional
                        };
                        
                        DataService.save(restUrl + 'designacion_vacacion_funcionario/', Util.parseSave(item))
                        .then(function(data) {
                            if (data) {
                                Message.success('El rango se creo exitosamente, imprima su calendario de vacaciones para ver su cronograma', 'Rango de vacación creada', 8000);
                                $mdDialog.hide();
                                obtenerVacaciones();
                            }
                        });
                    }
                    return true;
                }
            };

            Modal.show(config);
        }        

        function imprimirCalendario($event) {
            DataService.pdf(reportUrl + 'reporte/vacaciones/')
            .then(function (data) {
                Modal.show({
                    event: $event,
                    data: data,
                    title: 'Mi calendario de vacaciones',
                    templateUrl : 'app/components/crudTable/dialog.pdf.html',
                    controller: ['$scope', '$mdDialog', 'data', 'title', PdfController]
                });
            });
        }

        function DialogControllerVacacion($scope, $mdDialog, data, fields, title, add, column, done, Util, FeriadosService, restUrl, Datetime) {
            var vm = $scope;

            vm.data = data;
            vm.fields = fields;
            vm.title = title;
            vm.add = add;
            vm.keys = Util.getKeys(fields);
            vm.column = column;
            vm.hide = $mdDialog.hide;
            vm.cancel = $mdDialog.cancel;

            var fecha = data.fecha_asignacion.split('-');
            data.fecha_asignacion = Datetime.dateLiteral(new Date(parseInt(fecha[0]), parseInt(fecha[1]) - 1, parseInt(fecha[2])));
            fecha = new Date(parseInt(fecha[0]) + 1, parseInt(fecha[1]) - 1, parseInt(fecha[2]) + 1);

            var minDate = new Date(data.gestion_vacacion, 0, 1);
            vm.minDate = Datetime.diff(minDate, fecha) > 0 ? minDate: fecha;
            vm.maxDate = new Date(data.gestion_vacacion, 11, 31);
            vm.diasLaborales = diasLaborales;

            var dias = {
                domingo: 0,
                lunes: 1,
                martes: 2,
                miercoles: 3,
                jueves: 4,
                viernes: 5,
                sabado: 6
            };

            activate();

            function activate() {
                getJefes();
                getFeriados();
                getDiasHabiles();
            }

            function getJefes() {
                var users = [];
                DataService.list(restUrl + 'todos_jefes/')
                .then(function(data) {
                    if (data) {
                        for (var i in data) {
                            if (data[i].habilitado_marcar && data[i].groups[0] == 2) {
                                users.push({
                                    value: data[i].id,
                                    name: data[i].username + ' - ' + data[i].first_name + ' ' + data[i].last_name
                                });
                            }
                        }
                        vm.jefes = users;
                        angular.element('input.md-search').on('keydown', function(ev) {
                            ev.stopPropagation();
                        });
                    }
                });
            }


            function getFeriados() {
                FeriadosService.getFeriadosGestion(data.gestion_vacacion)
                .then(function (response) {
                    if (response && response.feriados) {
                        var filter = {};
                        var feriados = response.feriados;
                        for (var key in feriados) {
                            filter[key] = true;
                        }
                        vm.feriados = feriados;
                        vm.filter = filter;                     
                    }
                });
            }

            function diasLaborales (date) {
                if (vm.filter) {                    
                    var d = date.getDay();
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                    var fecha = year + '-' + ((month < 10 ? '0' : '') + month) + '-' + ((day < 10 ? '0' : '') + day);
                    return typeof vm.filter[fecha] == 'undefined' && vm.dias.indexOf(d) === -1;
                } else {
                    return true;
                }
            }

            function getDiasHabiles() {
                DataService.get(restUrl + 'dias_laborales/')
                .then(function (response) {
                    if (response) {
                        var laborales = response;
                        vm.dias = [];
                        for (var key in laborales) {
                            if (!laborales[key]) {
                                vm.dias.push(dias[key]);                                
                            }
                        }
                    }
                });
            }

            vm.answer = function(answer) {
                done(answer, $mdDialog);
            };

            vm.searchTerm = '';
            vm.clearSearchTerm = function() {
                vm.searchTerm = '';
            };

            vm.searchTerm2 = '';
            vm.clearSearchTerm2 = function() {
                vm.searchTerm2 = '';
            };
        }


    }

})();
