var autoSave = null;
var autoSaveBD = null;

(function() {
    'use strict';

    angular
    .module('app')
    .controller('FormularioController', FormularioController);

    /** @ngInject */
    function FormularioController($scope, $rootScope, Datetime, SideNavFactory, DataService, restUrl, Util, BreadcrumbFactory, Storage, Modal, $window, Message, reportUrl) {
        var vm = this;

        vm.gestion = Datetime.now('YYYY');
        vm.nombreEntidad = 'AGENCIA DE GOBIERNO ELECTRÓNICO Y TECNOLOGÍAS DE INFORMACIÓN Y COMUNICACIÓN';

        vm.getData = getData;
        vm.max = 8;
        vm.siguiente = siguiente;
        vm.anterior = anterior;

        vm.fields = {
            'ficha_personal': null,
            'ficha_trayectoria': null,
            'ficha_estudios_universitarios': null,
            'ficha_estudios_especializacion': null,
            'ficha_cursos_relacionados': null,
            'ficha_idiomas': null,
            'ficha_conocimientos': null,
            'ficha_experiencia_laboral': null
        };

        vm.fieldsFilter = {
            'ficha_personal': [
                'id',
                'email_personal',
                'documento_tipo',
                'documento_numero',
                'documento_exp',
                'telefono_domicilio',
                'telefono_celular',
                'fecha_nacimiento',
                'pais_nacimiento',
                'lugar_nacimiento',
                'nacionalidad',
                'zona_domicilio',
                'calle_domicilio',
                'nro_domicilio',
                'edificio',
                'piso',
                'nr_departamento',
                'nro_cuenta',
                'estado_civil',
                'grupo_sanguineo'
            ],
            'ficha_laboral': [
                'id',
                'cargo_actual',
                'unidad_actual',
                'profesion',
                'registro_profesional',
                'nro_funcionario_carrera'
            ]
        };


        vm.item = {
            trayectoria: {
                'id': '',
                'fecha_inicio': '',
                'fecha_final': '',
                'cargo': '',
                'tipo_trayectoria': ''
            },
            estudios_universitarios: {
                'id': '',
                'year_inicio': '',
                'year_final': '',
                'carrera': '',
                'universidad': '',
                'concluido': '',
                'nivel': '',
                'ciudad': '',
                'titulo': '',
                'fecha_emision': ''
            },
            estudios_especializacion: {
                'id': '',
                'year_inicio': '',
                'year_final': '',
                'nivel': '',
                'area': '',
                'carga_horaria': '',
                'universidad': '',
                'concluido': ''
            },
            cursos_relacionados: {
                'id': '',
                'mes': '',
                'year': '',
                'curso': '',
                'area': '',
                'carga_horaria': '',
                'universidad': '',
                'concluido': '',
                'titulo': ''
            },
            idiomas: {
                'id': '',
                'descripcion': '',
                'lee': '',
                'escribe': '',
                'habla': ''
            },
            conocimientos: {
                'id': '',
                'descripcion': '',
                'grado': ''
            },
            experiencia_laboral: {
                'id': '',
                'nombre': '',
                'tipo': '',
                'pais': '',
                'cargo': '',
                'fecha_inicio': '',
                'fecha_final': ''
            }
        };

        vm.data = {
            "personal": {
                "id" : "",
                "email_personal" : "",
                "documento_tipo" : "",
                "documento_numero" : "",
                "documento_exp" : "",
                "telefono_domicilio" : "",
                "telefono_celular" : "",
                "fecha_nacimiento" : "",
                "pais_nacimiento" : "",
                "lugar_nacimiento" : "",
                "nacionalidad" : "",
                "zona_domicilio" : "",
                "calle_domicilio" : "",
                "nro_domicilio" : "",
                "edificio" : "",
                "piso" : "",
                "nr_departamento" : "",
                "nro_cuenta" : "",
                "estado_civil" : "",
                "grupo_sanguineo" : ""
            },
            "laboral": {
                "profesion": "",
                "registro_profesional": "",
                "nro_funcionario_carrera": ""
            },
            "trayectoria": [],
            "estudios_universitarios": [],
            "estudios_especializacion": [],
            "cursos_relacionados": [],
            "idiomas": [],
            "conocimientos": [],
            "experiencia_laboral": []
        };

        vm.agregar = agregar;
        vm.eliminar = eliminar;
        vm.guardar = guardar;
        vm.imprimir = imprimir;
        vm.getInitial = getInitial;
        vm.user = Storage.getUser();

        var ficha_personal = restUrl + 'ficha_personal_historico/';

        activate();

        function activate() {
            BreadcrumbFactory.setParent('Mi cuenta');
            BreadcrumbFactory.setCurrent('');

            for (var key in vm.fields) {
                getFields(key);
            }

            // if (Storage.exist(ficha_personal)) {
            //     var data = Storage.get(ficha_personal);
            //     traverse(data, function (key, value, obj) {
            //         obj[key] = convertDate(value);
            //     });
            //     vm.data = data;
            // }

            loadData();

            $window.setTimeout(function () {
                // console.log('iniciando autoguardado');
                initAutoSave();
            }, 10*1000);
        }

        function save(autosave) {
            autosave = autosave || false;
            DataService.put(ficha_personal + vm.id_ficha + '/', {
                ficha_personal: JSON.stringify(vm.data),
                usuario: vm.user.id
            })
            .then(function (response) {
                 if (response && autosave) {
                    Message.success('Información guardada correctamente.');
                 }
            });
        }

        function convertDate(value) {
            if (typeof value == 'string') {
                var pos = value.indexOf('T');
                if (pos != -1 && pos <= 10 && pos >= 8) {
                    var fecha = value.substring(0, pos);
                    if (!/[a-zA-Z]+/g.test(fecha) && /^-?[0-9.]+\-?[0-9]+\-?[0-9]*$/g.test(fecha)) {
                        return new Date(value);
                    }
                }
            }
            return value;
        }

        function traverse(o, func) {
            for (var i in o) {
                func.apply(vm, [i, o[i], o]);
                if (o[i] !== null && typeof(o[i]) == "object") {
                    //going on step down in the object tree!!
                    traverse(o[i], func);
                }
            }
        }

        function loadData() {
            DataService.get(ficha_personal)
            .then(function (response) {
                if (response) {
                    if (response.count) {
                        var ficha = response.results[0];
                        var data = JSON.parse(ficha.ficha_personal);
                        traverse(data, function (key, value, obj) {
                            obj[key] = convertDate(value);
                        });
                        vm.data = data;
                        vm.id_ficha = ficha.id;
                        // console.log('recuperado', vm.id_ficha);
                    } else {
                        DataService.post(ficha_personal, {
                            ficha_personal: JSON.stringify(vm.data),
                            usuario: vm.user.id
                        })
                        .then(function (response) {
                            // console.log('nuevo', response);
                            if (response.id) {
                                vm.id_ficha = response.id;
                            }
                        });
                    }
                }
            });
        }

        function saveStorage() {
            Storage.set(ficha_personal, vm.data);
        }

        function initAutoSave() {
            /** Auto guardado en localStorage */
            autoSave = $window.setInterval(function () {
                saveStorage();
                // console.log('save!', vm.data);
            }, 30*1000); // 30 segundos

            /** Auto guardado en el backend */
            autoSaveBD = $window.setInterval(function () {
                if (vm.data != Storage.get(ficha_personal)) {
                    guardar();
                }
                // console.log('save BD!', vm.data);
            }, 1*60*1000); // 1 minutos
        }

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            $window.clearInterval(autoSave);
            $window.clearInterval(autoSaveBD);
            // guardar();
            // console.log('saliendo y guardando!');
        });

        function guardar(autosave) {
            saveStorage();
            save(autosave);
        }

        function imprimir($event) {
            DataService.pdf(reportUrl + 'reporte/fichapersonal/')
            .then(function (data) {
                Modal.show({
                    event: $event,
                    data: data,
                    title: 'Mi ficha personal',
                    templateUrl : 'app/components/crudTable/dialog.pdf.html',
                    controller: ['$scope', '$mdDialog', 'data', 'title', PdfController]
                });
            });
        }

        function PdfController($scope, $mdDialog, data, title) {
            var vm = $scope;

            vm.title = title;
            vm.data = data;
            vm.hide = $mdDialog.hide;
            vm.cancel = $mdDialog.cancel;
        }

        function agregar(tipo) {
            vm.data[tipo].push(createItem(vm.item[tipo], tipo));
            save();
            // console.log(vm.data);
        }

        function createItem(item, tipo) {
            var newItem = {};

            for (var i in item) {
                if (i == 'id') {
                    newItem.id_item = vm.user.username + '-' + tipo + '-' + ("0000" + (Math.random() * Math.pow(36, 4) << 0).toString(36)).slice(-4);
                } else {
                    newItem[i] = item[i];
                }
            }
            return newItem;
        }

        function eliminar(tipo, item, index) {
            Modal.confirm('¿Está seguro de eliminar el registro?', function () {
                vm.data[tipo].splice(index, 1);
                save();
                // console.log(vm.data);
            });
        }

        function getInitial () {
            var firstName = SideNavFactory.getUser().first_name;
            return firstName.length ? firstName[0].toUpperCase() : '?';
        }

        function getFields(table) {
            DataService.options(restUrl + table + '/')
            .then(function (response) {
                if (response) {
                    vm.fields[table] = Util.filterFields(DjangoRestFormly.toFormly(response.actions.POST), vm.fieldsFilter[table] || []);

                    if (table == 'ficha_personal') {
                        vm.fields.ficha_laboral = Util.filterFields(DjangoRestFormly.toFormly(response.actions.POST), vm.fieldsFilter.ficha_laboral);
                        // console.log('vm.fields.ficha_laboral', vm.fields.ficha_laboral);
                        vm.fields.ficha_laboral = Util.addPropertiesFormly(vm.fields.ficha_laboral, [{
                            'key': 'cargo_actual',
                            'defaultValue': vm.getData('cargo')
                        }, {
                            'key': 'unidad_actual',
                            'defaultValue': vm.getData('unidad_dependencia')
                        }]);
                    }
                }
            });
        }

        function getData(key) {

            var value = SideNavFactory.getUser()[key];
            if (['last_login', 'date_joined'].indexOf(key) != -1 ) {
                return Datetime.datetimeLiteral(new Date(value));
            } else if (['fecha_asignacion'].indexOf(key) != -1 && value) {
                var fecha = value.split('-');
                return Datetime.dateLiteral(new Date(parseInt(fecha[0]), parseInt(fecha[1]) - 1, parseInt(fecha[2])));
            }

            return value;
        }


        function siguiente() {
            var index = (vm.selectedIndex == vm.max) ? 0 : vm.selectedIndex + 1;
            vm.selectedIndex = index;
            // console.log(vm.selectedIndex);
        }

        function anterior() {
            var index = (vm.selectedIndex === 0) ? 0 : vm.selectedIndex - 1;
            vm.selectedIndex = index;
        }

    }
})();
