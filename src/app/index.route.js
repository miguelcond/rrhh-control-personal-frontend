(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/modules/dashboard/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('usuarios', {
        url: '/usuarios',
        templateUrl: 'app/modules/admin/usuarios/modules.admin.usuarios.html',
        controller: 'UsuariosController',
        controllerAs: 'usuarios'
      })
      .state('horarios', {
        url: '/horarios',
        templateUrl: 'app/modules/parameters/horarios/modules.parameters.horarios.html',
        controller: 'HorariosController',
        controllerAs: 'horarios'
      })
      .state('horarioRangos', {
        url: '/horarioRangos',
        templateUrl: 'app/modules/parameters/horarioRangos/modules.parameters.horarioRangos.html',
        controller: 'HorarioRangosController',
        controllerAs: 'horarioRangos'
      })
      .state('designacionHorarios', {
        url: '/designacionHorarios',
        templateUrl: 'app/modules/parameters/designacionHorarios/modules.parameters.designacionHorarios.html',
        controller: 'DesignacionHorariosController',
        controllerAs: 'designacionHorarios'
      })
      .state('registroHorarios', {
        url: '/registroHorarios',
        templateUrl: 'app/modules/parameters/registroHorarios/modules.parameters.registroHorarios.html',
        controller: 'RegistroHorariosController',
        controllerAs: 'registroHorarios'
      })
      .state('feriados', {
        url: '/feriados',
        templateUrl: 'app/modules/parameters/feriados/modules.parameters.feriados.html',
        controller: 'FeriadosController',
        controllerAs: 'feriados'
      })
      .state('tipoPermisos', {
        url: '/tipoPermisos',
        templateUrl: 'app/modules/parameters/tipoPermisos/modules.parameters.tipoPermisos.html',
        controller: 'TipoPermisosController',
        controllerAs: 'tipoPermisos'
      })
      .state('designacionPermisos', {
        url: '/designacionPermisos',
        templateUrl: 'app/modules/parameters/designacionPermisos/modules.parameters.designacionPermisos.html',
        controller: 'DesignacionPermisosController',
        controllerAs: 'designacionPermisos'
      })
      .state('designacionPermisosAdmin', {
        url: '/designacionPermisosAdmin',
        templateUrl: 'app/modules/parameters/designacionPermisosAdmin/modules.parameters.designacionPermisosAdmin.html',
        controller: 'DesignacionPermisosAdminController',
        controllerAs: 'designacionPermisosAdmin'
      })
      .state('designacionPermisosJefe', {
        url: '/designacionPermisosJefe',
        templateUrl: 'app/modules/parameters/designacionPermisosJefe/modules.parameters.designacionPermisosJefe.html',
        controller: 'DesignacionPermisosJefeController',
        controllerAs: 'designacionPermisosJefe'
      })
      .state('listaReporte', {
        url: '/listaReporte',
        templateUrl: 'app/modules/reports/listaReporte/modules.reports.listaReporte.html',
        controller: 'ListaReporteController',
        controllerAs: 'listaReporte'
      })
      .state('sincronizar', {
        url: '/sincronizar',
        templateUrl: 'app/modules/asistencia/sincronizar/modules.asistencia.sincronizar.html',
        controller: 'SincronizarController',
        controllerAs: 'sincronizar'
      })
      .state('configuracion', {
        url: '/configuracion',
        templateUrl: 'app/modules/parameters/configuracion/modules.parameters.configuracion.html',
        controller: 'ConfiguracionController',
        controllerAs: 'configuracion'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/modules/admin/login/modules.admin.login.html',
        controller: 'LoginController',
        controllerAs: 'login'
      })
      // .state('profile', {
      //   url: '/profile',
      //   templateUrl: 'app/modules/admin/profile/modules.admin.profile.html',
      //   controller: 'ProfileController',
      //   controllerAs: 'profile'
      // })
      .state('unidad', {
        url: '/unidad',
        templateUrl: 'app/modules/parameters/unidad/modules.parameters.unidad.html',
        controller: 'UnidadController',
        controllerAs: 'unidad'
      })
      .state('listaConsolidado', {
        url: '/listaConsolidado',
        templateUrl: 'app/modules/reports/listaConsolidado/modules.reports.listaConsolidado.html',
        controller: 'ListaConsolidadoController',
        controllerAs: 'consolidado'
      })
      .state('biometricos', {
        url: '/biometricos',
        templateUrl: 'app/modules/parameters/biometricos/modules.parameters.biometricos.html',
        controller: 'BiometricosController',
        controllerAs: 'biometricos'
      })
      .state('revisionHorarios', {
        url: '/revisiones',
        templateUrl: 'app/modules/parameters/revisionHorarios/modules.parameters.revisionHorarios.html',
        controller: 'RevisionHorariosController',
        controllerAs: 'revisionHorarios'
      })
      .state('sugerencia', {
        url: '/sugerencia',
        templateUrl: 'app/modules/admin/sugerencia/modules.admin.sugerencia.html',
        controller: 'SugerenciaController',
        controllerAs: 'sugerencia'
      })
      .state('profile', {
        url: '/profile',
        templateUrl: 'app/modules/formulario/modules.formulario.html',
        controller: 'FormularioController',
        controllerAs: 'formulario'
      })
      .state('designacionVacaciones', {
        url: '/designacionVacaciones',
        templateUrl: 'app/modules/parameters/designacionVacaciones/modules.parameters.designacionVacaciones.html',
        controller: 'DesignacionVacacionesController',
        controllerAs: 'designacionVacaciones'
      })
    $urlRouterProvider.otherwise('/');
  }

})();
