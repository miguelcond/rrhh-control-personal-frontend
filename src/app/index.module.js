(function() {
  'use strict';

  angular
    .module('app', ['ngSanitize', 'ui.router', 'ngMaterial', 'toastr', 'chart.js', 'ui.calendar', 'ncy-angular-breadcrumb', 'md.data.table', 'formlyMaterial', 'satellizer', 'scDateTime']);

})();
