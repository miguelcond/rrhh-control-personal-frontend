(function() {
    'use strict';

    angular
    .module('app')
    .component('crudTable', {
        templateUrl : 'app/components/crudTable/crudTable.html',
        controller : ['$scope', 'DataService', 'Message', 'Modal', 'Util', '$timeout', 'ArrayUtil', 'Datetime', CrudTableController],
        controllerAs: 'vm',
        bindings: {
            url: '=',
            fields: '=',
            fks: '=',
            titleTable: '=',
            template: '=',
            permission: '=',
            column: '=',
            formly: '=',
            editable: '=',
            eventSave: '=',
            dialogController: '=',
            order: '=',
            optionSelected: '=',
            buttons: '=',
            idShow: '=',
            node: '@'
        }
    });

    function CrudTableController($scope, DataService, Message, Modal, Util, $timeout, ArrayUtil) {
        var vm = this;
        var bookmark;
        var limit = 15;

        vm.headers = [];
        vm.items = [];
        vm.item = null;
        // vm.node = true; // Habilitar para nodejs

        vm.add = add;
        vm.edit = edit;
        vm.delete = deleteItem;
        vm.print = print;
        vm.save = save;
        vm.saveAll = saveAll;
        vm.removeFilter = removeFilter;
        vm.showFilter = showFilter;
        vm.getItems = getItems;
        vm.buttonEvent = buttonEvent;
        vm.dataGrid = {};

        vm.permissions = {
            create: true,
            read: true,
            update: true,
            delete: true,
            print: false,
            filter: true
        }

        // vm.limitOptions = [buttonEventlimit, 25, 50];
        vm.limitOptions = [limit];

        vm.options = {
            rowSelection: false,
            multiSelect: false,
            autoSelect: true,
            decapitate: false,
            largeEditDialog: true,
            boundaryLinks: true,
            limitSelect: true,
            pageSelect: false
        }

        vm.query = {
            order: '',
            limit: limit,
            page: 1,
            filter: ''
        }

        vm.filter = {
            options: {
                debounce: 500
            }
        };

        activate();

        function activate() {
            getFields();
        }

        function getItems () {
            if (vm.fieldsData) {
                vm.promise = getData();
            }
        }

        function add (event, id) {
            var add = typeof id == 'undefined';
            if (!add) {
                getItem(event, add, id);
            } else {
                openDialog(event, add, {});
            }
        }

        function save (data) {

            var obj = {};

            for (var i in data) {
                obj[i] = vm.dataGrid[data[i]]
            }

            DataService.save(vm.url, Util.parseSave(obj))
            .then(function(data) {
                if (data) {
                    Message.success('Registro actualizado');
                }
            });
        }

        function saveAll () {
            var data = vm.dataGrid,
                c = 0,
                length = vm.headers.length,
                dataSave = [],
                obj = {},
                promises = [];

            for (var i in data) {
                var key = i.split('_');
                key.shift();
                key = key.join('_');
                obj[key] = data[i];
                c++;
                if (c % length == 0) {
                    dataSave.push(obj);
                    promises.push(savePromise(obj));
                    obj = {};
                }
            }
            Promise.all(promises)
            .then(function (response) {
                for (var i in response) {
                    if (response[i] === false) {
                        Message.error('Se produjo un error al grabar todos los datos.');
                        throw new Error('Error al grabar todos los datos');
                    }
                }
                Message.success('Se guardaron todos los datos');
            });
        }

        function savePromise(data) {
            return new Promise(function (resolve, reject) {
                DataService.save(vm.url, Util.parseSave(data))
                .then(function(data) {
                    if (data) {
                        resolve(true);
                    } else {
                        reject(false);
                    }
                }).catch(function () {
                    reject(false);
                });
            });
        }

        function edit (event, item) {
            vm.add(event, item)
        }

        function deleteItem (event, id) {
            Modal.confirm('Se eliminará el registro de forma permanente, ¿Está seguro?.', function () {
                DataService.delete(vm.url, id)
                .then(function() {
                    Message.success('El registro se eliminó correctamente.');
                    vm.getItems();
                })
                .catch(function () {
                    Message.error();
                });
            }, null, event);
        }

        function getItem(event, add, id) {
            DataService.get(vm.url, id)
            .then(function(data) {
                if (data && data.id) {
                    openDialog(event, add, Util.filterItem(data));
                }
            });
        }

        function openDialog(event, add, item) {
            var config = {
                event: event,
                data: item,
                title: add ? 'Agregar' : 'Editar',
                add: add,
                fields: vm.fieldsData,
                column: vm.column || 1,
                templateUrl : vm.template || 'app/components/crudTable/dialog.crudTable.html',
                done : function (answer, $mdDialog) {
                    if (answer == 'save') {
                        DataService.save(vm.url, Util.parseSave(item))
                        .then(function(data) {
                            if (data) {
                                Message.success();
                                $mdDialog.hide();
                                vm.getItems();
                                if (vm.eventSave) {
                                    vm.eventSave(data);
                                }
                            }
                        });
                    }
                }
            };
            if (vm.dialogController) {
                config.controller = vm.dialogController;
            }
            Modal.show(config);
        }

        function getData() {
            return DataService.list(vm.url, vm.query)
            .then(function(data) {
                if (data) {
                    var items = data;
                    items.data = filterItems(data.results);
                    vm.items = items;
                }
            });
        }

        function getFields() {
            DataService.fields(vm.url)
            .then(function(data) {
                if (data) {
                    if (vm.node) {
                        vm.fieldsData = Util.filterFields(data, vm.fields);
                    } else {
                        // Dando formato a formly desde un Rest Django
                        vm.fieldsData = Util.filterFields(DjangoRestFormly.toFormly(data.actions.POST), vm.fields);
                    }
                    vm.fieldsData = Util.addPropertiesFormly(vm.fieldsData, vm.formly);
                    addOptionSelected(vm.fieldsData, vm.optionSelected);
                    DataService.setFormly(vm.fieldsData);

                    setHeaders(vm.fieldsData);
                    if (vm.editable) {
                        vm.types = Util.getKeys(vm.fieldsData);
                    }
                    if (vm.permission) {
                        vm.permissions = angular.merge(vm.permissions, vm.permission);
                    }
                    vm.getItems();
                }
            });
        }

        function addOptionSelected(data, options) {
            for (var i in data) {
                if (data[i].type == 'select') {
                    var item = Util.searchField(options, data[i].key);
                    if (item) {
                        ArrayUtil.insert(data[i].templateOptions.options, 0, {
                            name: item.text || 'Seleccione',
                            value: item.value || ''
                        });
                    }
                }
            }
        }

        function filterItems(data) {
            var fields = typeof vm.fields != 'undefined';
            var fks = typeof vm.fks != 'undefined';
            var array = [];
            for (var i in data) {
                for (var j in data[i]) {
                    if (fields && vm.fields.indexOf(j) == -1) {
                        delete data[i][j];
                    } else {
                        if (typeof vm.editable == 'undefined') {
                            if (typeof data[i][j] == 'boolean') {
                                data[i][j] = data[i][j] ? 'check_circle_success' : 'check_circle_gray';
                            } else if (fks && vm.fks.indexOf(j) != -1) {
                                if (Util.toType(data[i][j]) == 'array') {
                                    var l = Util.lengthOptions(vm.fieldsData, j);
                                    if (l > 1 && l == data[i][j].length) {
                                        data[i][j] = 'Todos';
                                    } else {
                                        var text = [];
                                        data[i][j].map(function (e) {
                                           text.push(Util.getFkData(vm.fieldsData ,j, e));
                                        });
                                        data[i][j] = text.join(', ');
                                    }
                                } else {
                                    data[i][j] = Util.getFkData(vm.fieldsData ,j, data[i][j]);
                                }
                            } else if (typeof data[i][j] == 'string' && !/[a-zA-Z]+/g.test(data[i][j]) && /^-?[0-9.]+\-?[0-9]+\-?[0-9]*$/g.test(data[i][j]) && data[i][j].length == 10) {
                                data[i][j] = Util.formatDate(data[i][j]);
                            } else if (Util.toType(data[i][j]) == 'array') {
                                data[i][j] = data[i][j][0];
                            } else if (typeof data[i][j] == 'string' && !/[a-zA-Z]+/g.test(data[i][j]) && /^-?[0-9.]+\:?[0-9]+\:?[0-9]*$/g.test(data[i][j]) && data[i][j].length == 8) {
                              data[i][j] = formatTime(data[i][j])
                            }
                        }
                    }
                }
                array.push(orderItem(data[i], vm.fieldsData, i));
            }
            return array;
        }

        function orderItem(data, fields, pos) {

            if (typeof fields == 'undefined' || fields.length == 0) {
                return data;
            }

            var item = {};

            for (var i in fields) {
                var field = fields[i].key
                if (typeof data[field] != 'undefined') {
                    if (vm.editable) {
                        if (typeof data[field] == 'string' && !/[a-zA-Z]+/g.test(data[field]) && /^-?[0-9.]+\-?[0-9]+\-?[0-9]*$/g.test(data[field]) && data[field].length == 10) {
                            var date = data[field].split('-');
                            vm.dataGrid[pos + '_' + field] = new Date(date[0], date[1]-1, date[2]);
                        } else {
                            vm.dataGrid[pos + '_' + field] = data[field];
                        }
                        item[field] = pos + '_' + field;
                    } else {
                        item[field] = data[field];
                    }
                }
            }

            return item;
        }

        function formatTime(time) {
            time = time.split(':')
            return [time[0], time[1]].join(':')
        }

        function setHeaders(data) {
            var headers = [];
            var order = typeof vm.order == 'undefined';
            for (var i in data) {
                headers.push({
                    name: data[i].key,
                    title: data[i].templateOptions.label,
                    order: order || (vm.order && vm.order.indexOf(data[i].key) != -1)
                });
            }
            vm.headers = headers;
        }

        function print(event, id) {
            DataService.pdf(vm.url + 'print/' + id + '/')
            .then(function (data) {
                Modal.show({
                    event: event,
                    data: data,
                    title: 'Impresión',
                    templateUrl : 'app/components/crudTable/dialog.pdf.html',
                    controller: ['$scope', '$mdDialog', 'data', 'title', PdfController]
                });
            });
        }

        function PdfController($scope, $mdDialog, data, title) {
            var vm = $scope;

            vm.title = title;
            vm.data = data;
            vm.hide = $mdDialog.hide;
            vm.cancel = $mdDialog.cancel;
        }

        function showFilter() {
            vm.filter.show = true;
            $timeout(function () {
                angular.element('#crud-table-input').focus();
            }, 300);
        }

        function removeFilter () {
            vm.filter.show = false;
            vm.query.filter = '';

            if(vm.filter.form.$dirty) {
                vm.filter.form.$setPristine();
            }
        }

        $scope.$watch('vm.query.filter', function (newValue, oldValue) {
            if(!oldValue) {
                bookmark = vm.query.page;
            }

            if(newValue !== oldValue) {
                vm.query.page = 1;
            }

            if(!newValue) {
                vm.query.page = bookmark;
            }

            vm.getItems();
        });

        //New Bottons
        function buttonEvent(event, id, callback) {
            callback(event, id);
        }

    }

})();
