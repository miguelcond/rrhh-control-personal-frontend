var autoSave = null;
var autoSaveBD = null;
(function() {
    'use strict'

    angular
    .module('app')
    .directive('acmeNavbar', acmeNavbar);

    /** @ngInject */
    function acmeNavbar() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/navbar/navbar.html',
            scope: {},
            controller: NavbarController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function NavbarController($auth, $window, $mdDialog, $location, Storage, SideNavFactory, Util, sincronizarUrl, Message, DataService, restUrl, $rootScope, $state) {
            var vm = this;

            vm.sincronizar = sincronizar;
            vm.refresh = refresh;

            activate();

            function activate() {
                if (Storage.existUser()) {
                    $rootScope.rrhh = Storage.getUser().groups[0] == 1;
                    if ($rootScope.rrhh) {
                        getUltimaSincronizacion();
                    }
                }
            }

            function sincronizar () {

                var url = sincronizarUrl;
                if (vm.fecha) {
                    if (vm.desde) {
                        url += '?fecha=' + Datetime.parseDate(vm.desde);
                    } else {
                        Message.warning('Debe seleccionar una fecha para la sincronización.');
                        return false;
                    }
                }
                DataService.get(url)
                .then(function (data) {
                    if (data) {
                        Message.success(data);
                        getUltimaSincronizacion();
                    }
                })
                .catch(function () {
                    Message.error('Ocurrió un error en la sincronización, vuelva a intentarlo más tarde');
                });

            }

            function getUltimaSincronizacion() {
                DataService.get(restUrl + 'registro_horarios_revision_ultimo/')
                .then(function (data) {
                    if (data.results) {
                        vm.ultima = Util.formatDate(data.results[0].fecha);
                    }
                });
            }

            vm.toggle = function () {
                angular.element('#sidenav-main').toggleClass('collapsed');
            };

            vm.openMenu = function ($mdOpenMenu, ev) {
                $mdOpenMenu(ev);
            };

            vm.profile = function () {
                $location.path("profile");
            };

            vm.settings = function () {
                $location.path("configuracion");
            };

            vm.getFirstName = function () {
                if (Storage.existUser()) {
                    return Storage.getUser().first_name;
                }
                return SideNavFactory.getUser().first_name;
            };

            vm.getColor = function () {
                return SideNavFactory.userColor;
            };

            vm.getInitial = function () {
                var firstName = SideNavFactory.getUser().first_name;
                return firstName.length ? firstName[0].toUpperCase() : '?';
            };

            vm.logout = function () {
                if (autoSave && autoSaveBD) {
                    $window.clearInterval(autoSave);
                    $window.clearInterval(autoSaveBD);
                }
                $auth.logout()
                .then(function() {
                    Storage.removeUser();
                    Storage.remove('menu');
                    Storage.remove('path');
                    $location.path("login");
                    $mdDialog.hide();
                });
            };

            vm.fullscreen = function () {
                angular.element('body').toggleClass('fullscreen');
                Util.fullscreen();
            };

            function refresh() {
                $mdDialog.hide();
                $state.reload();
            }
        }
    }

})();
