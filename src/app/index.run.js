var autoSave = null;
var autoSaveBD = null;
var TIME, sessionInterval;
(function() {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

    /** @ngInject */
    function runBlock(Storage, $location, $log, PageNoLogin, $window, timeSessionExpired, Datetime, $auth, $mdDialog) {

        var $container = angular.element('body');
        var $document = $window.document;

        // Collapsed Panel
        $container.on('click', '.btn-collapsed', function () {
            var $el = angular.element(this);
            $el.parent().parent().next().slideToggle();
            $el.toggleClass('rotate');
        });

        var $toasts = angular.element('#toast-container-main');
        $toasts.on('click', '.md-toast-close', function () {
            angular.element(this).parent().parent().parent().fadeOut();
        });

        // Redirect

        if (!Storage.existUser()) {
            var path = $location.path();
            if (!exist(path)) {
                if (path.length) {
                    Storage.set('path', path.replace('/', ''));
                }
                $location.path('login');
            }
        } else {
            if (typeof TIME != 'undefined') {
                TIME = timeSessionExpired*60;
                sessionInterval = $window.setInterval(function () {
                    TIME--;
                    if (TIME <= 0) {
                        $window.clearInterval(sessionInterval);
                        logout();
                    }
                }, 1000);
            }
        }

        function logout () {
            if (autoSave && autoSaveBD) {
                $window.clearInterval(autoSave);
                $window.clearInterval(autoSaveBD);
            }
            $auth.logout()
            .then(function() {
                Storage.set('expired', true);
                Storage.removeUser();
                Storage.remove('menu');
                Storage.remove('path');
                $location.path("login");
                $mdDialog.hide();
                $window.clearInterval(sessionInterval);
            });
        }

        function exist(path) {
            var paths = PageNoLogin;
            for (var i in paths) {
                if (path.indexOf('/' + paths[i]) === 0 || path.indexOf('/' + paths[i] + '/') === 0) {
                    return true;
                }
            }
            return false;
        }

        //Fullscreen
        function exitFullScreen () {
            angular.element('body').removeClass('fullscreen');
        }

        angular.element($window.document).on('keyup', function(e) {
            if (e.keyCode == 27) {
                exitFullScreen();
            }
        });

        $document.addEventListener("mozfullscreenchange", function () {
            if (!$document.mozFullScreen) {
                exitFullScreen();
            }
        }, false);

    }

})();
