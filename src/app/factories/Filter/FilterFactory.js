(function() {
    'use strict';

    angular
    .module('app')
    .factory('Filter', FilterFactory)
    .directive('integer', function(Filter) {
        return function(scope, element, attrs) {
            element.bind("keydown", function(event) {
                Filter.integer(event);
            });
        };
    })
    .directive('decimal', function(Filter) {
        return function(scope, element, attrs) {
            element.bind("keydown", function(event) {
                Filter.decimal(event);
            });
        };
    })
    .directive('numeric', function(Filter) {
        return function(scope, element, attrs) {
            element.bind("keydown", function(event) {
                Filter.numeric(event);
            });
        };
    })
    .directive('alpha', function(Filter) {
        return function(scope, element, attrs) {
            element.bind("keydown", function(event) {
                Filter.alpha(event);
            });
        };
    })
    .directive('alphaNumeric', function(Filter) {
        return function(scope, element, attrs) {
            element.bind("keydown", function(event) {
                Filter.alphaNumeric(event);
            });
        };
    })
    .directive('alphaDash', function(Filter) {
        return function(scope, element, attrs) {
            element.bind("keydown", function(event) {
                Filter.alphaDash(event);
            });
        };
    })
    .directive('uppercase', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ctrl) {
                ctrl.$parsers.push(function(input) {
                    return input ? input.toUpperCase() : "";
                });
                element.css("text-transform","uppercase");
            }
        };
    })
    .directive('nit', function(Filter) {
        var $error = angular.element('<div class="md-error-nit">NIT inválido</div>');

        return function(scope, element, attrs) {
            element.bind("keydown", function(event) {
                Filter.numeric(event);
            }).bind("keyup", function(event) {
                var isNit = Filter.isNit(element[0].value);
                var $element = angular.element(element[0]).parent();
                if (isNit) {
                    $element.removeClass('md-input-invalid');
                    $element.children('.md-error-nit').remove();
                } else {
                    $element.addClass('md-input-invalid');
                    $element.append($error);
                }
            });
        };
    })
    .directive('onlyDigits', function() {
      return {
        require: 'ngModel',
        scope: {
          odCantidad: '=',
          odMinimo: '=',
          odMaximo: '='
        },
        link: function(scope, element, attr, ngModelCtrl) {
          function fromUser(text) {
            var texto = String(text);
            if (texto) {
              var transformedInput = texto.replace(/[^0-9]/g, '');
              if (transformedInput !== texto) {
                ngModelCtrl.$setViewValue(transformedInput);
                ngModelCtrl.$render();
              }
              if (transformedInput && transformedInput.length > 0 && scope.odCantidad) {
                transformedInput = transformedInput.substr(0, scope.odCantidad);
                ngModelCtrl.$setViewValue(transformedInput);
                ngModelCtrl.$render();
              }
              if (transformedInput && transformedInput.length > 0 && scope.odMaximo) {
                if (transformedInput > scope.odMaximo) {
                  transformedInput = transformedInput.substr(0, (transformedInput.length - 1));
                  ngModelCtrl.$setViewValue(transformedInput);
                  ngModelCtrl.$render();
                }
              }
              return transformedInput;
            }
            return undefined;
          }
          ngModelCtrl.$parsers.push(fromUser);
        }
      };
    });

    // md-input-container.md-default-theme.md-input-invalid .md-input, md-input-container.md-input-invalid .md-input

    function FilterFactory() {

        var exceptions = {
            integer      : [46, 8, 9, 27, 13, 110, 190, 173],
            natural      : [46, 8, 9, 27, 13, 110, 190],
            decimal      : [46, 8, 9, 27, 13, 110, 188, 190, 173],
            numeric      : [46, 8, 9, 27, 13],
            alpha        : [46, 8, 9, 27, 13],
            alpha_dash   : [46, 8, 9, 27, 13, 173]
        };
        exceptions.alpha_numeric = exceptions.alpha;

        var evaluate = {
            numeric : function (e) {
                return (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105);
            },
            alpha : function (e) {
                return e.shiftKey || (e.keyCode < 65 || e.keyCode > 90);
            }
        };

        evaluate.integer = evaluate.numeric;
        evaluate.decimal = evaluate.numeric;
        evaluate.natural = evaluate.numeric;
        evaluate.alpha_numeric = function (e) {
            return evaluate.alpha(e) && evaluate.numeric(e);
        };
        evaluate.alpha_dash = evaluate.alpha_numeric;

        var nit = {
            verhoeff : function (x, y) {
                var m = ['0123456789', '1234067895', '2340178956', '3401289567', '4012395678', '5987604321', '6598710432', '7659821043', '8765932104', '9876543210'];
                if (m[x] && m[x][y]) {
                    return parseInt(m[x][y]);
                }
                return -1;
            },

            permutations : function (x, y)  {
                var m = ['0123456789', '1576283094', '5803796142', '8916043527', '9453126870', '4286573901', '2793806415', '7046913258'];
                if (m[x] && m[x][y]) {
                    return parseInt(m[x][y]);
                }
                return -1;
            },

            evaluate : function (num) {
                var check = 0, x, y, z;

                if (typeof num == 'string' && isNaN(num)) {
                    return false;
                }
                num = parseInt(num);
                for (var i = 0, l = num.toString().length; i < l; i++) {
                    x = i % 8;
                    y = num % 10;
                    num = Math.floor(num/10);
                    z = nit.permutations(x, y);
                    check = nit.verhoeff(check, z);
                }

                return check === 0;
            }
        };

        var factory = {
            isNumber: isNumber,
            isFloat: isFloat,
            integer: integer,
            isInteger: isInteger,
            decimal: decimal,
            isDecimal: isDecimal,
            natural: natural,
            isNatural: isNatural,
            numeric: numeric,
            isNumeric: isNumeric,
            alpha: alpha,
            isAlpha: isAlpha,
            alphaNumeric: alphaNumeric,
            isAlphaNumeric: isAlphaNumeric,
            alphaDash: alphaDash,
            isAlphaDash: isAlphaDash,
            removeTagHTML: removeTagHTML,
            empty: empty,
            isEmail: isEmail,
            convert: convert,
            isNit: isNit
        };

        return factory;

        function filterBase(e, type) {
            if (exceptions[type].indexOf(e.keyCode) !== -1 || (e.keyCode == 86 && e.ctrlKey === true) || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if (evaluate[type](e)) {
                e.preventDefault();
            }
        }

        function isNumber(o) {
            return typeof o === 'number' && isFinite(o);
        }

        function isFloat(value) {
            return value % 1 !== 0;
        }

        function integer(e) {
            filterBase(e, 'integer');
        }

        function isInteger(value) {
            if (/[a-zA-Z]+/g.test(value) || !/^-?[0-9.]*$/g.test(value)) {
                return false;
            }
            value = convert(value);
            if (value === 'NaN' || !isNumber(value)) {
                return false;
            }
            return !isFloat(value);
        }

        function decimal(e) {
            filterBase(e, 'decimal');
        }

        function isDecimal(value) {
            if (/[a-zA-Z]+/g.test(value) || !/^-?[0-9.]+\,?[0-9]*$/g.test(value)) {
                return false;
            }
            value = convert(value);
            if (value === 'NaN') {
                return false;
            }
            return isNumber(value);
        }

        function natural(e) {
            filterBase(e, 'natural');
        }

        function isNatural(value) {
            if (/[a-zA-Z]+/g.test(value) || !/^[0-9]*$/g.test(value)) {
                return false;
            }
            value = convert(value);
            if (value === 'NaN' || !isNumber(value)) {
                return false;
            }
            return !isFloat(value) && value >= 0;
        }

        function numeric(e) {
            filterBase(e, 'numeric');
        }

        function isNumeric(value) {
            return /^([0-9])*$/.test(value);
        }

        function alpha(e) {
            filterBase(e, 'alpha');
        }

        function isAlpha(value) {
            return /^[\u00F1A-Za-z]*$/.test(value);
        }

        function alphaNumeric(e) {
            filterBase(e, 'alpha_numeric');
        }

        function isAlphaNumeric(value) {
            return /^[\u00F1A-Za-z0-9]*$/.test(value);
        }

        function alphaDash(e) {
            filterBase(e, 'alpha_dash');
        }

        function isAlphaDash(value) {
            return /^[\u00F1A-Za-z-_0-9]*$/.test(value);
        }

        function removeTagHTML(text) {
            return text.replace(/<([^ >]+)[^>]*>.*?<\/\1>|<[^\/]+\/>/gi, "");
        }

        function empty(value) {
            return value === null || value.length === 0 || /^\s+$/.test(value);
        }

        function isEmail(value) {
            return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
        }

        function convert(value) {
            return parseFloat(isNaN(value) ? value.replace(/\./g, '').replace(',', '.') : value);
        }

        function isNit(value) {
            return nit.evaluate(value);
        }

    }
})();
