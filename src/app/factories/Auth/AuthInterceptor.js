var autoSave = null;
var autoSaveBD = null;
var TIME, sessionInterval;
(function() {
    'use strict'

    angular
    .module('app')
    .factory('AuthInterceptor', AuthInterceptor)
    .config(function($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    });

    /** @ngInject */
    function AuthInterceptor($q, $location, timeSessionExpired, $injector, $window) {

        var interceptor = {
            request: function(config) {

                // Set new Time
                if (typeof TIME != 'undefined') {
                    TIME = timeSessionExpired*60;
                }

                angular.element('#loading-progress').show();

                return config;
            },
            requestError: function(config) {
                return config;
            },
            response: function(response){
                angular.element('#loading-progress').hide();
                return response || $q.when(response);
            },
            responseError: function(rejection) {
                if (rejection.status === 401 || rejection.status === 403) {
                    // var $auth = $injector.get('$auth');
                    // var Modal = $injector.get('Modal');
                    // var Message = $injector.get('Message');
                    // var Storage = $injector.get('Storage');
                    // var SideNavFactory = $injector.get('SideNavFactory');

                    // var data = {
                    //  username: '',
                    //  password: ''
                    // };

                    // Modal.show({
                    //  templateUrl: 'app/modules/admin/login/login.dialog.html',
                    //  data: data,
                    //  clickOutsideToClose: false,
                    //  escapeToClose: false,
                    //  done : function (answer) {
                    //      if (answer == 'login') {
                    //          $auth.login({
                    //              username: data.username,
                    //              password: data.password
                    //          })
                    //          .then(function(response) {
                    //              var user = response.data.user;
                    //              SideNavFactory.setUser(user);
                    //              Storage.setUser(user);
                    //              $window.location = $location.absUrl();
                    //          })
                    //          .catch(function() {
                    //              Message.error('Su Nombre de usuario y Contraseña no son válidos.');
                    //          });
                    //      }
                    //  }
                    // });

                    var Storage = $injector.get('Storage');
                    var $auth = $injector.get('$auth');

                    Storage.set('path', $location.path());
                    $auth.logout()
                    .then(function() {
                        if (autoSave && autoSaveBD) {
                            $window.clearInterval(autoSave);
                            $window.clearInterval(autoSaveBD);
                        }
                        var $mdDialog = $injector.get('$mdDialog');
                        Storage.removeUser();
                        Storage.remove('menu');
                        $location.path("login");
                        $mdDialog.hide();

                        $window.clearInterval(sessionInterval);
                    });
                }
                return $q.reject(rejection);
            }
        };

        function exist(path) {
            var paths = $injector.get('PageNoLogin');
            for (var i in paths) {
                if (path.indexOf('/' + paths[i]) != -1 || path.indexOf('/' + paths[i] + '/') != -1) {
                    return true;
                }
            }
            return false;
        }

        return interceptor;
    }

})();
