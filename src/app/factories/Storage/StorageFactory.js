(function() {
    'use strict';

    angular
    .module('app')
    .factory('Storage', ['$window', 'appName', StorageFactory]);

    /** @ngInject */
    function StorageFactory($window, appName) {

        appName = appName || 'app';

        var $local = $window.localStorage;
        var $session = $window.sessionStorage;
        var $JSON = $window.JSON;

        var factory = {
            get: get,
            set: set,
            remove: remove,
            destroy: remove,
            exist: exist,
            setUser: setUser,
            getUser: getUser,
            existUser: existUser,
            removeUser: removeUser,
            setFlash: set,
            getFlash: getFlash
        };

        return factory;

        function setUser(user) {
            setSession('user', user);
        }

        function getUser() {
            return getSession('user');
        }

        function existUser() {
            return existSession('user');
        }

        function removeUser() {
            removeSession('user');
        }

        // LocalStorage
        function set(key, value) {
            $local.setItem(appName + '_' + key, $JSON.stringify(value));
        }

        function get(key) {
            if (exist(key)) {
                return $JSON.parse($local.getItem(appName + '_' + key));
            }
            return null;
        }

        function remove(key) {
            $local.removeItem(appName + '_' + key);
        }

        function exist(key) {
            var value = $local.getItem(appName + '_' + key);
            return typeof value != 'undefined' && value != 'undefined' && value !== null && value != 'null' && value != '[]';
        }

        // SessionStorage
        function setSession(key, value) {
            $session.setItem(appName + '_' + key, $JSON.stringify(value));
        }

        function getSession(key) {
            return $JSON.parse($session.getItem(appName + '_' + key));
        }

        function removeSession(key) {
            $session.removeItem(appName + '_' + key);
        }

        function existSession(key) {
            var value = $session.getItem(appName + '_' + key);
            return typeof value != 'undefined' && value != 'undefined' && value !== null && value != 'null' && value != '[]';
        }

        // Flash Data
        function getFlash(key) {
            if (exist(key)) {
                var value = get(key);
                remove(key);
                return value;
            }
            return false;
        }
    }
})();
