(function() {
    'use strict'

    angular
    .module('app')
    .factory('Message', ['$mdToast', MessageFactory]);

    /** @ngInject */
    function MessageFactory($mdToast) {

        var factory = {
            success : success,
            warning : warning,
            error : error
        };

        return factory;

        function success(msg, title, timeout) {
            var data = {
                icon : 'check',
                message : msg || 'La operación se realizó correctamente.',
                title : title || 'Correcto',
                type : 'success',
                timeout: timeout
            };

            render(data);
        }

        function error(msg, title, timeout) {
            var data = {
                icon : 'error',
                message : msg || 'Ocurrió un error al procesar su operación.',
                title : title || 'Error',
                type : 'danger',
                timeout: timeout
            };

            render(data);
        }

        function warning(msg, title, timeout) {
            var data = {
                icon : 'warning',
                message : msg || 'Ocurrió algo inesperado al procesar su operación.',
                title : title || 'Advertencia',
                type : 'warning',
                timeout: timeout
            };

            render(data);
        }

        function render(data) {
            $mdToast.show({
                controller: ToastController,
                templateUrl: 'app/factories/Messages/layout.toast.html',
                parent : angular.element('#toast-container-main'),
                hideDelay: typeof data.timeout != 'undefined' ? (data.timeout == 0 ? 31536000000 : data.timeout) : 5000,
                position: 'top right',
                locals: data
            });
        }

        function ToastController($scope, $mdToast, icon, message, title, type) {
            var vm = $scope;

            vm.icon = icon;
            vm.message = message;
            vm.title = title;
            vm.type = type;

            vm.close = function() {
                $mdToast.hide()
            };
        }
    }
})();