# Instalación de la Aplicación

## Configuración del Servidor
Para una correcta instalación, el servidor debe tener las siguientes configuraciones obligatoriamente:

> [SERVER.md](SERVER.md)

## Instalación del proyecto
### Descargar el proyecto de gitlab

Clonar el proyecto por **HTTPS**:
```sh
git clone https://gitlab.agetic.gob.bo/agetic/rrhh-control-personal-frontend.git
```

### Instalación
Ingresar al directorio del proyecto
```sh
cd rrhh-control-personal-frontend
```

#### Instalando dependencias bower
```sh
bower install
```

**Nota.-** En caso de no tener los permisos necesarios instalar con el usuario `sudo`, Ejemplo:
```sh
sudo bower install
```
si sigue teniendo problemas de permisos usar:
```sh
sudo bower install --allow-root
```

**Nota.-** Al momento de instalar las dependencias `bower` aparecerá en la instalación unos cuestionarios para seleccionar la versión de ciertas librerías, seleccionar la versión que haga referencia a `app`.

#### Instalando dependencias npm
```sh
npm install
```
**Nota.-** En caso de no tener los permisos necesarios instalar con el usuario `sudo`, Ejemplo:
```sh
sudo npm install
```
#### Copiar archivos de dependencias modificadas
Django-rest-formly
```sh
cp lib/django-rest-formly.js bower_components/django-rest-formly/
```

Sc-date-time
```sh
cp lib/sc-date-time.js bower_components/sc-date-time/dist/
```

#### Configurar los datos de conexión a los servicios REST del backend

Crear el archivo `index.constants.js` desde el archivo de ejemplo `index.constants.js.sample` :
```sh
cp src/app/index.constants.js.sample src/app/index.constants.js
```

Edite el archivo `index.constants.js`
```sh
nano src/app/index.constants.js
```

Actualice las siguientes líneas:
```
    .constant("authUrl", "BACKEND_URL/api-token-auth/")
    .constant("restUrl", "BACKEND_URL/api/v2/")
    .constant("sincronizarUrl", "BACKEND_URL/revision/horario/")
    .constant("reportUrl", "BACKEND_URL")
    .constant("calendarUrl", "FERIADOS_URL")
    .constant("biometricoUrl", "BIOMETRICOS_URL/api/v1/registros")
```

Reemplace lo siguiente:
* `BACKEND_URL` por la url del backend [rrhh-control-personal-backend](https://gitlab.agetic.gob.bo/agetic/rrhh-control-personal-backend).
* `FERIADOS_URL` por la url del microservicio feriados [rrhh-feriados-backend](https://gitlab.agetic.gob.bo/agetic/rrhh-feriados-backend).
* `BIOMETRICOS_URL` por la url del microservicio de marcardo biométrico [rrhh-bitometricos](https://gitlab.agetic.gob.bo/agetic/rrhh-biometricos).


## Iniciar el proyecto en modo Desarrollo
```sh
gulp serve
```

**Nota.-** En caso de no tener los permisos necesarios instalar con el usuario `sudo`, Ejemplo:
```sh
sudo gulp serve
```

## Iniciar el proyecto en modo Producción
Es necesario construir los archivos minificados del proyecto
```sh
npm run build
```

**Nota.-** En caso de no tener los permisos necesarios instalar con el usuario `sudo`, Ejemplo:
```sh
sudo npm run build
```

### Opción 1: Iniciar con Express
1. Iniciar el proyecto con los archivos minificados con express (archivo server.js)
```sh
npm start
```

2. Ingresar a la url cuando se inicie
```
http://<SERVIDOR>:3100
```

### Opción 2: NGINX o Apache
1. Renombrar la capeta `dist` por `produccion`
```sh
mv dist produccion
```

2. Crear un enlace simbolico en la carpeta raiz del servidor web
```sh
sudo ln -s /home/usuario/rrhh-control-personal-frontend/produccion /var/www/html/personal
```

2. Ingresar a la url
```
http://<SERVIDOR>/personal
```