Actualización del proyecto para Producción
===============================
#### Actualizando nuevos archivos
    git pull origin master
    
#### Actualizando dependencias bower
    bower install

#### Actualizando dependencias npm
    npm install

#### Copiar archivos de dependencias modificadas solo si se hizo npm install

Django-rest-formly

    Copiar el archivo:
    /path-del-proyecto/lib/django-rest-formly.js 
    
    en la carpeta: 
    /path-del-proyecto/bower_components/django-rest-formly/

Sc-date-time

    Copiar el archivo:
    /path-del-proyecto/lib/sc-date-time.js 
    
    en la carpeta: 
    /path-del-proyecto/bower_components/sc-date-time/dist/

#### Construir los archivos minificados del proyecto
    gulp build

#### Iniciar el proyecto con los archivos minificados con express (archivo server.js) siempre y cuando este servicio no haya sido iniciado previamente
    npm start