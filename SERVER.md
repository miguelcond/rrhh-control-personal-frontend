# Instalación General en el Servidor

## Build Essentials
Instalar build essentials
```sh
sudo apt-get install build-essential
```

## Instalación de Nodejs
Visitar la página https://github.com/nodesource/distributions#debinstall para su instalación o para la instalación manual ir a https://github.com/nodesource/distributions#debmanual, se debe instalar la versión 6.9.x (LTS) de Nodejs.

### Ubuntu / Debian

#### Actualizando el sistema operativo (Ubuntu, Debian)
```sh
sudo apt-get update
```

#### Para Ubuntu
```sh
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
```
#### Para Debian
```sh
curl -sL https://deb.nodesource.com/setup_6.x | sudo bash -
sudo apt-get install -y nodejs
```
## Instalación de npm
```sh
sudo npm install -g npm
```
## Instalación de bower
```sh
npm install bower -g
```

**NOTA.-** En caso de no tener los permisos necesarios instalar con el usuario sudo, Ejemplo:
```sh
sudo npm install bower -g
```

## Instalación del Proyecto

Para continuar con la instalación del PROYECTO, seguir los pasos del siguiente archivo:

> [INSTALL.md](INSTALL.md)
